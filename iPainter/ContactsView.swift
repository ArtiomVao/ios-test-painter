//
//  ContactsView.swift
//  PhotoPainter
//
//  Created by Artem Shapovalov on 14.11.2024.
//

import SwiftUI
import Contacts

struct ContactsView: View {
  var openEditor: (CNContact) -> Void = { _ in }
  var hide: () -> Void = {}
  @State private var contacts: [CNContact] = []
  @State private var searchText = ""
  private var filteredContacts: [CNContact] {
    if searchText.isEmpty {
      return contacts
    }
    return contacts.filter {
      $0.givenName.localizedStandardContains(searchText)
      || $0.familyName.localizedStandardContains(searchText)
    }
  }
  
  var body: some View {
    NavigationView {
      LazyVStack {
        ForEach(filteredContacts, id: \.identifier) { contact in
          Button(
            action: {
              hide()
              openEditor(contact)
            },
            label: {
              HStack {
                _avatar(from: contact.imageData)
                Text("\(contact.givenName) \(contact.familyName)")
                  .foregroundStyle(.foreground)
                Spacer()
              }
            }
          )
          .padding(.horizontal, 20)
        }
        .searchable(text: $searchText)
        Spacer()
      }
      .frame(
        minWidth: 0,
        maxWidth: .infinity,
        minHeight: 0,
        maxHeight: .infinity,
        alignment: .topLeading
      )
      .task {
        let contactStore = CNContactStore()
        var allContainers: [CNContainer] = []
        
        do {
          allContainers = try contactStore.containers(matching: nil)
        } catch {
          print("Error fetching containers")
          contacts = []
          return
        }
        
        let keysToFetch = [
          CNContactIdentifierKey,
          CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
          CNContactImageDataAvailableKey,
          CNContactThumbnailImageDataKey,
          CNContactImageDataKey
        ] as? [any CNKeyDescriptor]
        
        guard let keysToFetch
        else {
          contacts = []
          return
        }
        
        var results: [CNContact] = []
        
        for container in allContainers {
          let fetchPredicate = CNContact.predicateForContactsInContainer(
            withIdentifier: container.identifier
          )
          
          do {
            let containerResults = try contactStore.unifiedContacts(
              matching: fetchPredicate,
              keysToFetch: keysToFetch
            )
            results += containerResults
          } catch {
            print("Error fetching results for container")
          }
        }
        
        contacts = results.filter {
          $0.imageDataAvailable && (!$0.familyName.isEmpty || !$0.givenName.isEmpty)
        }
      }
      .navigationTitle("Изменить аватар контакта")
      .navigationBarItems(
        leading: Button("Закрыть") { hide() }
      )
    }
  }
  
  @ViewBuilder
  private func _avatar(from data: Data?) -> some View {
    if let data, let img = UIImage(data: data) {
      Image(uiImage: img)
        .resizable()
        .aspectRatio(contentMode: .fit)
        .frame(width: 30, height: 30)
    }
  }
}

final class ContactsViewController: UIHostingController<ContactsView> {
  
  init(openEditor: @escaping (CNContact) -> Void) {
    super.init(rootView: ContactsView())
    rootView.openEditor = openEditor
    rootView.hide = { [weak self] in self?.hide() }
  }
  
  @MainActor required dynamic init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func hide() {
    dismiss(animated: true, completion: nil)
  }
}
