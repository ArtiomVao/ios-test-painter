//
//  Entrance.swift
//  iPainter
//
//  Created by Artem Shapovalov on 15.10.2022.
//

import SwiftUI
import Photos

struct Entrance: View {
  
  let completion: () -> ()
  
  var body: some View {
    VStack {
      EntranceLogo()
        .frame(width: 150, height: 150)
      Text("Access Your Photos and Videos")
        .font(.system(size: 20, weight: .semibold))
        .foregroundColor(.white)
        .padding()
      Button {
        PHPhotoLibrary.checkAuthorizationStatus {
          guard $0 else {
            return
          }
          
          completion()
        }
      } label: {
        Text("Allow Access").foregroundColor(.white).padding()
      }
      .buttonStyle(MainButtonStyle())
    }
    .padding()
  }
}
