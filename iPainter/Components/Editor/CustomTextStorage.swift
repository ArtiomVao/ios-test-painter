//
//  CustomTextStorage.swift
//  iPainter
//
//  Created by Artem Shapovalov on 30.10.2022.
//

import UIKit

class CustomTextStorage: NSTextStorage {
  
  private let _imp: NSTextStorage
  
  override init() {
    _imp = NSTextStorage()
    super.init()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override var string: String {
    _imp.string
  }
  
  override func attributes(
    at location: Int,
    effectiveRange range: NSRangePointer?
  ) -> [NSAttributedString.Key : Any] {
    return _imp.attributes(at: location, effectiveRange: range)
  }
  
  override func replaceCharacters(in range: NSRange, with str: String) {
    beginEditing()
    _imp.replaceCharacters(in: range, with: str)
    edited(.editedCharacters,
           range: range,
           changeInLength: str.count - range.length)
    endEditing()
  }
  
  override func replaceCharacters(in range: NSRange,
                                  with attrString: NSAttributedString) {
    beginEditing()
    _imp.replaceCharacters(in: range, with: attrString)
    edited(.editedCharacters,
           range: range,
           changeInLength: attrString.length - range.length)
    endEditing()
  }
  
  override func setAttributes(_ attrs: [NSAttributedString.Key : Any]?,
                              range: NSRange) {
    beginEditing()
    _imp.setAttributes(attrs, range: range)
    edited(.editedAttributes, range: range, changeInLength: 0)
    endEditing()
  }
}
