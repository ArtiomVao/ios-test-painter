//
//  ColorPickerButton.swift
//  iPainter
//
//  Created by Artem Shapovalov on 16.10.2022.
//

import SwiftUI

struct ColorPickerButton: View {
  
  var action: () -> ()
  
  @ObservedObject private var _model = ToolsModel.shared
  
  private var _color: UIColor {
    if let shapeModel = _model.shapeModel {
      return shapeModel.color
    }
    if let textModel = _model.textModel {
      return textModel.color
    }
    return _model.getColor()
  }
  
  var body: some View {
    Button { action() } label: {
      ZStack {
        Image("color_wheel")
          .renderingMode(.original)
          .resizable()
          .aspectRatio(contentMode: .fit)
          .contrast(0.6)
          .frame(width: 40, height: 40)
        Circle()
          .fill(Color(_color))
          .frame(width: 23, height: 23)
      }
    }
  }
}
