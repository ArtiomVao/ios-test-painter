// swiftlint:disable all
//
//  EditorViewController.swift
//  iPainter
//
//  Created by Artem Shapovalov on 15.10.2022.
//

import UIKit
import SwiftUI
import PencilKit
import Contacts

class EditorViewController: UIViewController {
  
  let imageView = UIImageView()
  
  let controlsView = UIView()
  var toolsView = UIView()
  var pkCanvas = PKCanvas()
  let colorPicker = UIColorPickerViewController()
  var shapeMenu = UIView()
  var eraserMenu = UIView()
  var tipMenu = UIView()
  let loadingView = UIView()
  
  let headerView = UIView()
  let undoButton = UIButton()
  let clearAllButton = UIButton()
  
  let cgCanvas = CGCanvasView()
  
  let gestureView = GestureView()
  var tapGestureRecognizer = UITapGestureRecognizer()
  var panGestureRecognizer = UIPanGestureRecognizer()
  var pinchGestureRecognizer = UIPinchGestureRecognizer()
  var rotationGestureRecognizer = UIRotationGestureRecognizer()
  
  let tooltipMenu = UIMenuController.shared
  
  var textEditor = TextEditorViewController()
  var fontSliderView = UIView()
  
  var fontSliderModel = FontSlider.Model()
  
  var videoURL: URL? = nil
  var collectionNeedsInsert: Bool = false
  var onDownloadComplete: (Bool) -> () = { _ in }
  var contact: CNContact?
  
  private var _updatingCanvas = false
  private var _dragBegan = false
  private var _initialDragPoint = CGPoint.zero
  private var _initialRotation: CGFloat? = nil
  private var _initialScale: CGFloat? = nil
  
  private var _deleteMenuItem: UIMenuItem {
    UIMenuItem(title: "Delete", action: #selector(_deleteSelected))
  }
  
  private var _editTextMenuItem: UIMenuItem {
    UIMenuItem(title: "Edit", action: #selector(_editSelectedText))
  }
  
  private var _moveForwardMenuItem: UIMenuItem {
    UIMenuItem(title: "Move Forward", action: #selector(_moveSelectedForward))
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = .black
    loadingView.backgroundColor = .clear
    
    // MARK: - SwiftUI views
    
    let tview = ToolsView() {
      self.pkCanvas.tool = $0
      self.cgCanvas.deselectAll()
      self._toggleCanvases()
    } changeWidth: {
      if let _ = ToolsModel.shared.shapeModel,
         let drawingId = ToolsModel.shared.editedDrawingId {
        self.cgCanvas.drawings[drawingId].line?.strokeWidth = $0
        self.cgCanvas.setNeedsDisplay()
        return
      }
      ToolsModel.shared.set(width: $0)
      self.pkCanvas.tool = ToolsModel.shared.selectedTool.pkTool
    } onEditTool: {
      if !$0 {
        self.fontSliderView.alpha = 0
        self.cgCanvas.deselectAll()
      }
    } presentColorPicker: {
      self._presentColorPicker()
    } toggleShapeMenu: {
      UIView.animate(withDuration: 0.3) {
        self.shapeMenu.layer.opacity = 1
        self.view.bringSubviewToFront(self.shapeMenu)
      }
    } toggleTipMenu: {
      UIView.animate(withDuration: 0.3) {
        self.tipMenu.layer.opacity = 1
        self.view.bringSubviewToFront(self.tipMenu)
      }
    } toggleEraserMenu: {
      UIView.animate(withDuration: 0.3) {
        self.eraserMenu.layer.opacity = 1
        self.view.bringSubviewToFront(self.eraserMenu)
      }
    } onEditorStyleChange: {
      if $0 == .text {
        self._presentTextEditor()
      } else {
        ToolsModel.shared.reset()
        self.cgCanvas.deselectAll()
        self._toggleCanvases()
        self.textEditor.dismiss(animated: true)
      }
    } onChangeTextModel: {
      self._changeText($0)
    } onChangeShapeModel: {
      if let m = ToolsModel.shared.shapeModel,
         let drawingId = ToolsModel.shared.editedDrawingId {
        self.cgCanvas.drawings[drawingId].shapeModel = m
        self.cgCanvas.setNeedsDisplay()
      }
    } download: {
      var videoImage: UIImage? = nil
      if let _ = self.videoURL {
        videoImage = self.cgCanvas.getPhoto()
      }
      
      guard let bgImage = self.imageView.image,
            let newPhoto = self.cgCanvas.getPhoto(background: bgImage) else {
        return
      }
      
      self.imageView.image = newPhoto
      
      if let vimage = videoImage, let vurl = self.videoURL {
        self._showLoadingView()
        
        let videoEncoder = VideoEncoder()
        videoEncoder.image = vimage
        videoEncoder.filePath = vurl.relativePath
        videoEncoder.onSaved = {
          DispatchQueue.main.async {
            ToolsModel.shared.downloading = false
            self._removeLoadingView()
            self.onDownloadComplete(self.collectionNeedsInsert)
            self._close()
          }
        }
        videoEncoder.startReadingAsset(imgSize: newPhoto.size)
        return
      }
      
      if let contact = self.contact?.mutableCopy() as? CNMutableContact {
        let req = CNSaveRequest()
        contact.imageData = newPhoto.pngData()
        req.update(contact)
        let store = CNContactStore()
        do {
          try store.execute(req)
        } catch {
          debugPrint("Error update contact")
        }
      }
      
      UIImageWriteToSavedPhotosAlbum(newPhoto,
                                     self,
                                     #selector(self._downloadComplete),
                                     nil)
    } close: {
      self._close()
    }
    
    toolsView = UIHostingController(rootView: tview, ignoresKeyboard: true).view
    toolsView.backgroundColor = .black
    
    let shapesMenu = ContextMenu(items: MenuItem.regularShapes) {
      self._hideShapeMenu()
      self.cgCanvas.add(shape: $0, shapeModel: ShapeModel())
      ToolsModel.shared.editorStyle = .shape
      ToolsModel.shared.selectedTool = .lasso
      let i = self.cgCanvas.drawings.count - 1
      ToolsModel.shared.editedDrawingId = i
      ToolsModel.shared.shapeModel = self.cgCanvas.drawings[i].shapeModel
      self.gestureView.isUserInteractionEnabled = true
      self._toggleCanvases()
      self._toggleHeaderAppearance()
    }
    
    if let sm = UIHostingController(rootView: shapesMenu).view {
      shapeMenu = sm
      shapeMenu.backgroundColor = .clear
      shapeMenu.layer.cornerRadius = 10
      shapeMenu.layer.masksToBounds = true
    }
    
    // MARK: - Eraser menu
    
    let erasersMenu = ContextMenu(items: MenuItem.erasers) {
      switch $0 {
      case .eraser: ToolsModel.shared.selectEraser(.eraser)
      case .blurEraser: ToolsModel.shared.selectEraser(.blurEraser)
      case .objectEraser: ToolsModel.shared.selectEraser(.objectEraser)
      default: break
      }
      self._toggleCanvases()
    }
    
    if let em = UIHostingController(rootView: erasersMenu).view {
      eraserMenu = em
      eraserMenu.backgroundColor = .clear
      eraserMenu.layer.cornerRadius = 10
      eraserMenu.layer.masksToBounds = true
    }
    
    // MARK: - Tip menu
    
    let tipsMenu = ContextMenu(items: MenuItem.penTips) {
      ToolsModel.shared.currentTip = $0
      self._toggleCanvases()
    }
    
    if let tm = UIHostingController(rootView: tipsMenu).view {
      tipMenu = tm
      tipMenu.backgroundColor = .clear
      tipMenu.layer.cornerRadius = 10
      tipMenu.layer.masksToBounds = true
    }
    
    // MARK: - Color picker
    
    colorPicker.modalPresentationStyle = .popover
    colorPicker.modalTransitionStyle = .coverVertical
    colorPicker.delegate = self
    
    // MARK: - Controls view
    
    view.addSubview(controlsView)
    
    controlsView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    controlsView.heightAnchor.constraint(equalToConstant: 190).isActive = true
    controlsView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    controlsView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    controlsView.translatesAutoresizingMaskIntoConstraints = false
    
    controlsView.addSubview(toolsView)
    
    // MARK: - Tools view
    
    toolsView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
    toolsView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
    toolsView.heightAnchor.constraint(equalToConstant: 100).isActive = true
    toolsView.centerYAnchor.constraint(equalTo: controlsView.centerYAnchor).isActive = true
    toolsView.translatesAutoresizingMaskIntoConstraints = false
    
    // MARK: - Header view
    
    view.addSubview(headerView)
    
    headerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    headerView.topAnchor.constraint(
      equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
    headerView.heightAnchor.constraint(equalToConstant: 60).isActive = true
    headerView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    headerView.translatesAutoresizingMaskIntoConstraints = false
    
    headerView.addSubview(undoButton)
    headerView.addSubview(clearAllButton)
    
    // MARK: - Undo button
    
    if let img = UIImage(named: "undo") {
      undoButton.setImage(img, for: .normal)
    }
    undoButton.layer.opacity = 0.5
    undoButton.addTarget(self, action: #selector(_undo), for: .touchDown)
    
    undoButton.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 10).isActive = true
    undoButton.centerYAnchor.constraint(
      equalTo: headerView.centerYAnchor).isActive = true
    undoButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
    undoButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
    undoButton.translatesAutoresizingMaskIntoConstraints = false
    
    // MARK: - Clear all button
    
    clearAllButton.setTitle("Clear All", for: .normal)
    clearAllButton.layer.opacity = 0.5
    clearAllButton.addTarget(self, action: #selector(_undoAll), for: .touchDown)
    
    clearAllButton.rightAnchor.constraint(equalTo: headerView.rightAnchor, constant: -10).isActive = true
    clearAllButton.centerYAnchor.constraint(
      equalTo: headerView.centerYAnchor).isActive = true
    clearAllButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
    clearAllButton.widthAnchor.constraint(equalToConstant: 70).isActive = true
    clearAllButton.translatesAutoresizingMaskIntoConstraints = false
    
    // MARK: - Image view
    
    view.addSubview(imageView)
    
    imageView.contentMode = .scaleAspectFit
    
    imageView.topAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
    imageView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    imageView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    imageView.bottomAnchor.constraint(equalTo: controlsView.topAnchor).isActive = true
    imageView.translatesAutoresizingMaskIntoConstraints = false
    
    // MARK: - PKCanvasView
    
    view.addSubview(pkCanvas)
    
    pkCanvas.onTouchesBegan = {
      self._handleTouches($0)
    }
    
    pkCanvas.backgroundColor = .clear
    pkCanvas.isOpaque = false
    pkCanvas.drawingPolicy = .anyInput
    pkCanvas.tool = ToolsModel.shared.selectedTool.pkTool
    pkCanvas.delegate = self
//    pkCanvas.drawingGestureRecognizer.addTarget(self, action: #selector(_drawing))
    // MARK: - CGCanvasView
    
    view.addSubview(cgCanvas)
    
    cgCanvas.backgroundColor = .clear
    cgCanvas.onTouchesBegan = {
      self._handleTouches($0)
    }
    cgCanvas.onTouchesEnded = {
      self._toggleHeaderAppearance()
    }
    
    cgCanvas.isUserInteractionEnabled = false
    
    // MARK: - Gesture view
    
    view.addSubview(gestureView)
    
    tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(_tap))
    panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(_drag))
    pinchGestureRecognizer = UIPinchGestureRecognizer(target: self,
                                                      action: #selector(_pinch))
    rotationGestureRecognizer = UIRotationGestureRecognizer(target: self,
                                                            action: #selector(_rotate))
    
    panGestureRecognizer.delegate = self
    pinchGestureRecognizer.delegate = self
    rotationGestureRecognizer.delegate = self
    
    gestureView.backgroundColor = .clear
    gestureView.addGestureRecognizer(tapGestureRecognizer)
    gestureView.addGestureRecognizer(panGestureRecognizer)
    gestureView.addGestureRecognizer(rotationGestureRecognizer)
    gestureView.addGestureRecognizer(pinchGestureRecognizer)
    gestureView.isUserInteractionEnabled = false
    
    // MARK: - Font slider
    
    let fontSlider = FontSlider(model: fontSliderModel) {
      if let textModel = ToolsModel.shared.textModel {
        let font = TextModel.font(of: $0, with: textModel.fontDesign.value)
        ToolsModel.shared.textModel?.size = $0
        ToolsModel.shared.textModel?.font = font
        self._changeText()
      }
    } onEnd: {
      guard let id = ToolsModel.shared.editedDrawingId else {
        return
      }
      
      self._presentTextEditor(self.cgCanvas.drawings[id].text)
    }
    
    fontSliderView = UIHostingController(rootView: fontSlider).view
    
    view.addSubview(fontSliderView)
    
    fontSliderView.alpha = 0
    fontSliderView.backgroundColor = .clear
    fontSliderView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    fontSliderView.centerYAnchor.constraint(
      equalTo: view.centerYAnchor, constant: -60).isActive = true
    fontSliderView.heightAnchor.constraint(equalToConstant: 170).isActive = true
    fontSliderView.widthAnchor.constraint(equalToConstant: 40).isActive = true
    fontSliderView.translatesAutoresizingMaskIntoConstraints = false
    
    // MARK: - Tooltip menu

    tooltipMenu.arrowDirection = .default
    
    // MARK: - Shape menu
    
    view.addSubview(shapeMenu)
    
    shapeMenu.layer.opacity = 0
    
    shapeMenu.heightAnchor.constraint(equalToConstant: 225).isActive = true
    shapeMenu.widthAnchor.constraint(equalToConstant: 200).isActive = true
    shapeMenu.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
    shapeMenu.bottomAnchor.constraint(equalTo: toolsView.topAnchor).isActive = true
    shapeMenu.translatesAutoresizingMaskIntoConstraints = false
    
    // MARK: - Eraser menu
    
    view.addSubview(eraserMenu)
    
    eraserMenu.layer.opacity = 0
    
    eraserMenu.heightAnchor.constraint(equalToConstant: 95).isActive = true
    eraserMenu.widthAnchor.constraint(equalToConstant: 200).isActive = true
    eraserMenu.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
    eraserMenu.bottomAnchor.constraint(equalTo: toolsView.bottomAnchor, constant: -40).isActive = true
    eraserMenu.translatesAutoresizingMaskIntoConstraints = false
    
    // MARK: - Tip menu
    
    view.addSubview(tipMenu)
    
    tipMenu.layer.opacity = 0
    
    tipMenu.heightAnchor.constraint(equalToConstant: 95).isActive = true
    tipMenu.widthAnchor.constraint(equalToConstant: 200).isActive = true
    tipMenu.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
    tipMenu.bottomAnchor.constraint(equalTo: toolsView.bottomAnchor, constant: -40).isActive = true
    tipMenu.translatesAutoresizingMaskIntoConstraints = false
    
    view.bringSubviewToFront(pkCanvas)
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    
    let rect = imageView.rectOfImage
    pkCanvas.frame    = rect
    cgCanvas.frame    = rect
    gestureView.frame = rect
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setNeedsStatusBarAppearanceUpdate()
  }
  override var preferredStatusBarStyle: UIStatusBarStyle {
    .lightContent
  }
  
  // MARK: - Navigation
  
  @objc private func _close() {
    dismiss(animated: true)
    ToolsModel.shared.reset()
    _undoAll()
  }
  
  @objc private func _downloadComplete(image: UIImage,
                                       didFinishSavingWithError: NSError,
                                       contextInfo: UnsafeRawPointer?) {
    DispatchQueue.main.async {
      ToolsModel.shared.downloading = false
    }
    onDownloadComplete(collectionNeedsInsert)
    _close()
  }
  
  private func _presentTextEditor(_ textToEdit: NSAttributedString? = nil,
                                  editText: Bool = false) {
    textEditor = TextEditorViewController(
      justChangingFont: textToEdit != nil && !editText,
      text: textToEdit
    )
    
    if textToEdit == nil {
      ToolsModel.shared.textModel = TextModel()
    }
    
    _toggleCanvases()
    
    textEditor.onDone = {
      self._showFontSlider()
      
      if let s = ToolsModel.shared.textModel?.size {
        self.fontSliderModel.value = 76 - s
      }
      
      guard let attrStr = self.textEditor.textView.attributedText else {
        return
      }
      
      let text = NSMutableAttributedString(attributedString: attrStr)
      
      text.removeAttribute(.underlineColor, range: NSMakeRange(0, text.length))
      text.removeAttribute(.underlineStyle, range: NSMakeRange(0, text.length))
      
      var rects: [CGFloat: CGRect] = [:]
      
      self.textEditor.bgRects.forEach {
        rects[$0.origin.y] = $0
      }
      
      if let _ = textToEdit, let i = ToolsModel.shared.editedDrawingId {
        self.cgCanvas.drawings[i].text = text
        self.cgCanvas.drawings[i].frame = CGRect(
          origin: self.cgCanvas.drawings[i].frame?.origin ?? .zero,
          size: text.size()
        )
        self.cgCanvas.drawings[i].textBg = Array(rects.values)
        self.cgCanvas.setNeedsDisplay()
      } else {
        self.cgCanvas.add(text: text,
                          in: CGRect(origin: .zero, size: text.size()),
                          bg: Array(rects.values))
        ToolsModel.shared.editedDrawingId = self.cgCanvas.drawings.count - 1
      }
      
      self.cgCanvas.isUserInteractionEnabled = false
      self.gestureView.isUserInteractionEnabled = true
      self._toggleHeaderAppearance()
    }

    textEditor.modalPresentationStyle = .overCurrentContext
    textEditor.modalTransitionStyle = .crossDissolve
    
    if let t = textToEdit {
      self.textEditor.textView.attributedText = t
      self.textEditor.updateTextView()
      
      if !editText {
        self.textEditor.view.alpha = 0
        
        present(textEditor, animated: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
          self.textEditor.onDone()
          self.textEditor.dismiss(animated: false)
        }
        return
      }
    }
    
    fontSliderView.alpha = 0
    present(textEditor, animated: true)
  }
  
  // MARK: - Undo
  
  @objc private func _undo() {
    cgCanvas.undo()
    _toggleHeaderAppearance()
    ToolsModel.shared.reset()
    _toggleCanvases()
  }
  
  @objc private func _undoAll() {
    undoButton.alpha     = 0.5
    clearAllButton.alpha = 0.5
    cgCanvas.clear()
    _toggleHeaderAppearance()
    ToolsModel.shared.reset()
    _toggleCanvases()
  }
  
  private func _toggleHeaderAppearance() {
    let canUndo = !cgCanvas.drawings.isEmpty
    undoButton.alpha     = canUndo ? 1 : 0.5
    clearAllButton.alpha = canUndo ? 1 : 0.5
  }
  
  // MARK: - Appearance
  
  override public var canBecomeFirstResponder: Bool {
    get {
      return true
    }
  }
  
  private func _showTooltipMenu(at point: CGPoint) {
    self.tooltipMenu.showMenu(
      from: self.cgCanvas,
      rect: CGRect(x: point.x - 30, y: point.y, width: 50, height: 50)
    )
  }
  
  private func _toggleCanvases() {
    if ToolsModel.shared.editorStyle == .text || ToolsModel.shared.editorStyle == .shape {
      cgCanvas.isUserInteractionEnabled = false
      gestureView.isUserInteractionEnabled = true
      view.bringSubviewToFront(gestureView)
      if ToolsModel.shared.editorStyle == .text {
        self._showFontSlider()
      }
      return
    }
    
    fontSliderView.alpha = 0
    
    switch ToolsModel.shared.selectedTool {
    case .neon, .eraser:
      cgCanvas.isUserInteractionEnabled = true
      gestureView.isUserInteractionEnabled = false
      view.bringSubviewToFront(cgCanvas)
    case .lasso, .objectEraser:
      cgCanvas.isUserInteractionEnabled = false
      gestureView.isUserInteractionEnabled = true
      view.bringSubviewToFront(gestureView)
    default:
      cgCanvas.isUserInteractionEnabled = false
      gestureView.isUserInteractionEnabled = false
      view.bringSubviewToFront(pkCanvas)
    }
  }
  
  private func _hideShapeMenu() {
    UIView.animate(withDuration: 0.3) {
      self.shapeMenu.layer.opacity = 0
      self.view.setNeedsDisplay()
    }
  }
  
  private func _hideEraserMenu() {
    UIView.animate(withDuration: 0.3) {
      self.eraserMenu.layer.opacity = 0
      self.view.setNeedsDisplay()
    }
  }
  
  private func _hideTipMenu() {
    UIView.animate(withDuration: 0.3) {
      self.tipMenu.layer.opacity = 0
      self.view.setNeedsDisplay()
    }
  }
  
  private func _handleTouches(_ touch: UITouch?) {
    if touch?.view != shapeMenu {
      _hideShapeMenu()
    }
    
    if touch?.view != eraserMenu {
      _hideEraserMenu()
    }
    
    if touch?.view != tipMenu {
      _hideTipMenu()
    }
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    _handleTouches(touches.first)
  }
  
  private func _showLoadingView() {
    view.addSubview(loadingView)
    loadingView.frame = view.frame
  }
  
  private func _removeLoadingView() {
    loadingView.removeFromSuperview()
  }
  
  private func _showFontSlider() {
    fontSliderView.alpha = 1
    view.bringSubviewToFront(fontSliderView)
  }
  
  private func _presentColorPicker() {
    present(colorPicker, animated: true)
  }
  
  // MARK: - Gestures
  
  @objc private func _tap(_ recognizer: UITapGestureRecognizer) {
    let tap = recognizer.location(in: cgCanvas)
    
    _determineSelection(for: tap) { i, d in
      ToolsModel.shared.reset()
      self.fontSliderView.alpha = 0
      self.tooltipMenu.menuItems = [self._deleteMenuItem, self._moveForwardMenuItem]
      if let m = d.textModel {
        ToolsModel.shared.textModel = m
        ToolsModel.shared.editorStyle = .text
        ToolsModel.shared.editedDrawingId = i
        self.tooltipMenu.menuItems?.insert(self._editTextMenuItem, at: 1)
        self._showFontSlider()
      }
      if self.cgCanvas.selectDrawing(i) {
        self._showTooltipMenu(at: tap)
      }
    } callbackLine: { i, d in
      self.fontSliderView.alpha = 0
      if self.cgCanvas.selectDrawing(i) {
//        UIMenuController.shared.menuItems = [self._deleteMenuItem, self._moveForwardMenuItem]
        self.tooltipMenu.menuItems = [self._deleteMenuItem, self._moveForwardMenuItem]
        self._showTooltipMenu(at: tap)
      }
      
      if let m = d.shapeModel {
        ToolsModel.shared.shapeModel = m
        ToolsModel.shared.editorStyle = .shape
        ToolsModel.shared.editedDrawingId = i
        return
      }
      
      ToolsModel.shared.reset()
    } callbackEnd: { d in
      if d == nil {
        self.cgCanvas.deselectAll()
        ToolsModel.shared.reset()
      }
      
      self._toggleCanvases()
    }
  }
  
  @objc private func _drag(_ recognizer: UIPanGestureRecognizer) {
    let point = recognizer.translation(in: cgCanvas)
    let loc = recognizer.location(in: cgCanvas)
    
    switch recognizer.state {
    case .began:
      gestureView.points = []
      _initialDragPoint = point
      _determineSelection(for: loc) { i, _ in
        if self.cgCanvas.selectedDrawings.count > 1 {
          self.cgCanvas.draggedDrawings = self.cgCanvas.selectedDrawings
        } else if self.cgCanvas.selectedDrawings.contains(i) {
          self.cgCanvas.addToDragged(i)
        }
      } callbackLine: { i, _ in
        if self.cgCanvas.selectedDrawings.count > 1 {
          self.cgCanvas.draggedDrawings = self.cgCanvas.selectedDrawings
        } else if self.cgCanvas.selectedDrawings.contains(i) {
          self.cgCanvas.addToDragged(i)
        }
      }
    case .changed:
      if ToolsModel.shared.selectedTool == .objectEraser {
        _determineSelection(for: loc) { i, _ in
          self.cgCanvas.delete(at: i)
        } callbackLine: { i, _ in
          self.cgCanvas.delete(at: i)
        }
        self._toggleHeaderAppearance()
        return
      }
      if cgCanvas.draggedDrawings.isEmpty {
        gestureView.points.append(loc)
        gestureView.setNeedsDisplay()
      }
      let deltaX = point.x - _initialDragPoint.x
      let deltaY = point.y - _initialDragPoint.y
      cgCanvas.draggedDrawings.forEach { i in
        if let _ = cgCanvas.drawings[i].frame {
          cgCanvas.drawings[i].frame?.origin.x += deltaX
          cgCanvas.drawings[i].frame?.origin.y += deltaY
          cgCanvas.drawings[i].imageOffset.x += deltaX
          cgCanvas.drawings[i].imageOffset.y += deltaY
          return
        }
        if let points = cgCanvas.drawings[i].line?.points {
          cgCanvas.drawings[i].line?.points = points.map { p in
            return CGPoint(x: p.x + deltaX, y: p.y + deltaY)
          }
        }
        if let arrow = cgCanvas.drawings[i].line?.arrow {
          cgCanvas.drawings[i].line?.arrow = arrow.map { p in
            return CGPoint(x: p.x + deltaX, y: p.y + deltaY)
          }
        }
        if cgCanvas.drawings[i].image != nil || cgCanvas.drawings[i].frame != nil {
          cgCanvas.drawings[i].imageOffset.x += deltaX
          cgCanvas.drawings[i].imageOffset.y += deltaY
        }
      }
      cgCanvas.setNeedsDisplay()
      _initialDragPoint = point
    case .ended, .cancelled:
      if cgCanvas.draggedDrawings.isEmpty {
        let ri = Utils.getRotation(for: gestureView.points)
        let rect = CGRect(x: ri.minX,
                          y: ri.minY,
                          width: ri.maxX - ri.minX,
                          height: ri.maxY - ri.minY)
        
        self._determineSelection(for: rect) { i, d in
          self.tooltipMenu.menuItems = [self._deleteMenuItem, self._moveForwardMenuItem]
          if let m = d.textModel {
            ToolsModel.shared.textModel = m
            ToolsModel.shared.editorStyle = .text
            ToolsModel.shared.editedDrawingId = i
            self.tooltipMenu.menuItems?.insert(self._editTextMenuItem, at: 1)
          } else {
            ToolsModel.shared.reset()
          }
          self.cgCanvas.selectDrawingAppend(i)
        } callbackLine: { i, d in
          self.cgCanvas.selectDrawingAppend(i)
          ToolsModel.shared.reset()
        }
      }
      
      gestureView.points = []
      gestureView.setNeedsDisplay()
      _initialDragPoint = .zero
      cgCanvas.draggedDrawings = []
    default: break
    }
  }
  
  @objc private func _pinch(_ recognizer: UIPinchGestureRecognizer) {
    let point = recognizer.location(in: cgCanvas)
    switch recognizer.state {
    case .began:
      _determineSelection(for: point) { i, d in
        if self.cgCanvas.selectedDrawings.count > 1 {
          self.cgCanvas.draggedDrawings = self.cgCanvas.selectedDrawings
          if let s = d.scale {
            self._initialScale = s
          }
        } else if self.cgCanvas.selectedDrawings.contains(i) {
          self.cgCanvas.addToDragged(i)
          if let s = d.scale {
            self._initialScale = s
          }
        }
      } callbackLine: { i, d in
        if self.cgCanvas.selectedDrawings.count > 1 {
          self.cgCanvas.draggedDrawings = self.cgCanvas.selectedDrawings
          if let s = d.scale {
            self._initialScale = s
          }
        } else if self.cgCanvas.selectedDrawings.contains(i) {
          self.cgCanvas.addToDragged(i)
          if let s = d.scale {
            self._initialScale = s
          }
        }
      }
    case .changed:
      cgCanvas.draggedDrawings.forEach { i in
        cgCanvas.drawings[i].scale = recognizer.scale
      }
      cgCanvas.setNeedsDisplay()
    case .ended, .cancelled:
      _initialScale = 0
    default: break
    }
  }
  
  @objc private func _rotate(_ recognizer: UIRotationGestureRecognizer) {
    let point = recognizer.location(in: cgCanvas)
    switch recognizer.state {
    case .began:
      _determineSelection(for: point) { i, d in
        if self.cgCanvas.selectedDrawings.count > 1 {
          self.cgCanvas.draggedDrawings = self.cgCanvas.selectedDrawings
          if let r = d.rotation {
            self._initialRotation = r
          }
        } else if self.cgCanvas.selectedDrawings.contains(i) {
          self.cgCanvas.addToDragged(i)
          if let r = d.rotation {
            self._initialRotation = r
          }
        }
      } callbackLine: { i, d in
        if self.cgCanvas.selectedDrawings.count > 1 {
          self.cgCanvas.draggedDrawings = self.cgCanvas.selectedDrawings
          if let r = d.rotation {
            self._initialRotation = r
          }
        } else if self.cgCanvas.selectedDrawings.contains(i) {
          self.cgCanvas.addToDragged(i)
          if let r = d.rotation {
            self._initialRotation = r
          }
        }
      }
    case .changed:
      cgCanvas.draggedDrawings.forEach { i in
        if let r = _initialRotation {
          if let m = cgCanvas.drawings[i].shapeModel, m.flipped == true {
            cgCanvas.drawings[i].rotation = r - recognizer.rotation
          } else {
            cgCanvas.drawings[i].rotation = r + recognizer.rotation
          }
        } else {
          cgCanvas.drawings[i].rotation = recognizer.rotation
        }
      }
      cgCanvas.setNeedsDisplay()
    case .ended, .cancelled:
      _initialRotation = 0
    default: break
    }
  }
  
  private func _determineSelection(
    for point:     CGPoint,
    callbackFrame: @escaping (Int, CGCanvasView.Drawing) -> () = {_, _ in},
    callbackLine:  @escaping (Int, CGCanvasView.Drawing) -> () = {_, _ in},
    callbackEnd:   @escaping (CGCanvasView.Drawing?) -> () = {_ in}
  ) {
    let d = cgCanvas.drawings.enumerated().sorted {
      $0.offset > $1.offset
    }.first { i, d in
      if let blendMode = d.line?.blendMode, blendMode == .clear {
        return false
      }
      
      var rLoc: CGPoint
      var ri = Utils.getRotation(for: [])
      
      if let p = d.line?.points {
        ri = Utils.getRotation(for: p)
      }
      
      if let r = d.rotation {
        if let f = d.frame {
          let nx = point.x - f.midX
          let ny = point.y - f.midY
          rLoc = CGPoint(x: nx * cos(r) + ny * sin(r) + f.midX,
                         y: -nx * sin(r) + ny * cos(r) + f.midY)
        } else {
          let nx = point.x - ri.midPoint.x
          let ny = point.y - ri.midPoint.y
          rLoc = CGPoint(x: nx * cos(r) + ny * sin(r) + ri.midPoint.x,
                         y: -nx * sin(r) + ny * cos(r) + ri.midPoint.y)
        }
      } else {
        rLoc = point
      }
      
      if let f = d.frame, f.contains(rLoc) {
        callbackFrame(i, d)
        return true
      }
      
      let contains = rLoc.x >= ri.minX && rLoc.y >= ri.minY && rLoc.x <= ri.maxX && rLoc.y <= ri.maxY
      
      if contains {
        callbackLine(i, d)
      }
      
      return contains
    }
    
    callbackEnd(d?.element)
  }
  
  private func _determineSelection(
    for rect:      CGRect,
    callbackFrame: @escaping (Int, CGCanvasView.Drawing) -> () = { _, _ in },
    callbackLine:  @escaping (Int, CGCanvasView.Drawing) -> () = { _, _ in }
  ) {
    let _ = cgCanvas.drawings.enumerated().sorted {
      $0.offset > $1.offset
    }.filter { i, d in
      if let blendMode = d.line?.blendMode, blendMode == .clear {
        return false
      }
      
      var ri = Utils.getRotation(for: [])
      
      if let p = d.line?.points {
        ri = Utils.getRotation(for: p)
      }
      
      let rLoc = [CGPoint(x: rect.minX, y: rect.minY),
                  CGPoint(x: rect.maxX, y: rect.minY),
                  CGPoint(x: rect.maxX, y: rect.maxY),
                  CGPoint(x: rect.minX, y: rect.maxY)].map { point in
        if let r = d.rotation {
          if let f = d.frame {
            let nx = point.x - f.midX
            let ny = point.y - f.midY
            return CGPoint(x: nx * cos(r) + ny * sin(r) + f.midX,
                           y: -nx * sin(r) + ny * cos(r) + f.midY)
          } else {
            let nx = point.x - ri.midPoint.x
            let ny = point.y - ri.midPoint.y
            return CGPoint(x: nx * cos(r) + ny * sin(r) + ri.midPoint.x,
                           y: -nx * sin(r) + ny * cos(r) + ri.midPoint.y)
          }
        } else {
          return point
        }
      }
      
      ri = Utils.getRotation(for: rLoc)
      
//      if let f = d.frame, f.contains(p) {
//        callbackFrame(i, d)
//        return true
//      }
      
      var containedPoints = 0
      
      if let dPoints = d.line?.points {
        dPoints.forEach {
          let dp = $0
          
          let contains = dp.x >= ri.minX && dp.y >= ri.minY && dp.x <= ri.maxX && dp.y <= ri.maxY
          
          if contains {
            containedPoints += 1
          }
        }
        
        if CGFloat(containedPoints) / CGFloat(dPoints.count) > 0.3 {
          callbackLine(i, d)
        }
      }
      
      return containedPoints > 0
    }
  }
  
  // MARK: - Operations on selected drawing
  
  @objc private func _moveSelectedForward() {
    cgCanvas.moveSelectedDrawingForward()
  }
  
  @objc private func _deleteSelected() {
    ToolsModel.shared.reset()
    _toggleCanvases()
    cgCanvas.deleteSelectedDrawings()
    _toggleHeaderAppearance()
  }
  
  @objc private func _editSelectedText() {
    guard let id = ToolsModel.shared.editedDrawingId else {
      return
    }
    
    _presentTextEditor(cgCanvas.drawings[id].text, editText: true)
  }
  
  // MARK: - Change text
  
  private func _changeText(_ isFontChanged: Bool = false) {
    
    guard let textModel = ToolsModel.shared.textModel,
          let i = ToolsModel.shared.editedDrawingId,
          let t = self.cgCanvas.drawings[i].text else {
      return
    }
    
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.alignment = textModel.alignment.value
    
    var attributes: [NSAttributedString.Key: Any] = [
      .paragraphStyle: paragraphStyle,
      .foregroundColor: textModel.color,
      .font: textModel.font]
    
    if textModel.bgStyle == .stroke {
      attributes[.strokeColor] = UIColor.black
      attributes[.strokeWidth] = -4
    }
    
    if isFontChanged {
      _presentTextEditor(self.cgCanvas.drawings[i].text)
      return
    }
    self.cgCanvas.drawings[i].text?.setAttributes(
      attributes,
      range: NSMakeRange(0, t.length)
    )
    if let s = self.cgCanvas.drawings[i].text?.size() {
      self.cgCanvas.drawings[i].frame?.size = s
    }
    self.cgCanvas.setNeedsDisplay()
  }
  
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    self.pkCanvas.tool = ToolsModel.shared.selectedTool.pkTool
  }
}

extension EditorViewController: PKCanvasViewDelegate {
  
  func canvasViewDrawingDidChange(_ canvasView: PKCanvasView) {
    _toggleHeaderAppearance()
    
    if _updatingCanvas {
      return
    }
    
    _updatingCanvas = true
    
    let image = canvasView.drawing.image(from: canvasView.bounds,
                                         scale: UIScreen.main.scale)
    
    self.cgCanvas.add(image: image, points: self.pkCanvas.points)
    self.pkCanvas.points = []
    
    self.pkCanvas.drawing = PKDrawing()
    self._updatingCanvas = false
  }
  
  func canvasViewDidEndUsingTool(_ canvasView: PKCanvasView) {
  }
}

extension EditorViewController: UIGestureRecognizerDelegate {
  
  func gestureRecognizer(
    _ gestureRecognizer: UIGestureRecognizer,
    shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer
  ) -> Bool {
    return true
  }
}

extension EditorViewController: UIViewControllerTransitioningDelegate {
  
  func presentationController(
    forPresented presented: UIViewController,
    presenting: UIViewController?,
    source: UIViewController
  ) -> UIPresentationController? {
    PresentationController(presentedViewController: presented,
                           presenting: presenting)
  }
}

extension UIView {
  
  func asImage() -> UIImage {
    let renderer = UIGraphicsImageRenderer(bounds: bounds)
    return renderer.image { rendererContext in
      layer.render(in: rendererContext.cgContext)
    }
  }
}

extension EditorViewController: UIColorPickerViewControllerDelegate {
  
  func colorPickerViewController(_ viewController: UIColorPickerViewController,
                                 didSelect color: UIColor,
                                 continuously: Bool)
  {
    if let shapeModel = ToolsModel.shared.shapeModel,
        let drawingId = ToolsModel.shared.editedDrawingId {
      shapeModel.color = color
      self.cgCanvas.drawings[drawingId].line?.color = color
      self.cgCanvas.setNeedsDisplay()
      return
    }
    if let textModel = ToolsModel.shared.textModel,
       let drawingId = ToolsModel.shared.editedDrawingId {
      textModel.color = color
      self.cgCanvas.drawings[drawingId].textModel = textModel
      self._changeText()
      self.cgCanvas.setNeedsDisplay()
      return
    }
    ToolsModel.shared.set(color: color)
    self.pkCanvas.tool = ToolsModel.shared.selectedTool.pkTool
  }
}
