//
//  FontSlider.swift
//  iPainter
//
//  Created by Artem Shapovalov on 24.10.2022.
//

import SwiftUI

struct FontSlider: View {
  
  class Model: ObservableObject {
    
    @Published var value: CGFloat
    
    init() {
      if let textModel = ToolsModel.shared.textModel {
        self.value = 76 - textModel.size
        return
      }
      self.value = 50
    }
  }
  
  @ObservedObject var model: Model
  var sliderRange: ClosedRange<Double> = 16...60
  var updateSize: (CGFloat) -> ()
  var onEnd: () -> () = {}
  
  @State private var _lastCoordinateValue: CGFloat = 0.0
  
  var body: some View {
    GeometryReader { gr in
      let thumbSize = gr.size.width * 0.8
      let minValue = CGFloat(0)
      let maxValue = gr.size.height - thumbSize
      
      let scaleFactor = (maxValue - minValue) / (sliderRange.upperBound - sliderRange.lowerBound)
      let lower = sliderRange.lowerBound
      let sliderVal = (model.value - lower) * scaleFactor + minValue
      
      ZStack {
        SliderBackgroundShape()
          .fill(.gray)
          .frame(width: 170, height: 30)
          .rotationEffect(.degrees(-90))
//          .padding(.horizontal, 5)
          .opacity(0.5)
        VStack {
          Circle()
            .foregroundColor(.white)
            .frame(width: thumbSize, height: thumbSize)
            .offset(y: sliderVal)
            .shadow(radius: 10)
            .gesture(
              DragGesture(minimumDistance: 0)
                .onChanged { v in
                  if (abs(v.translation.height) < 0.1) {
                    _lastCoordinateValue = sliderVal
                  }
                  if v.translation.height > 0 {
                    let nextCoordinateValue = min(
                      maxValue, _lastCoordinateValue + v.translation.height
                    )
                    model.value = ((nextCoordinateValue - minValue) / scaleFactor) + lower
                  } else {
                    let nextCoordinateValue = max(
                      minValue, _lastCoordinateValue + v.translation.height
                    )
                    model.value = ((nextCoordinateValue - minValue) / scaleFactor) + lower
                  }
                  
                  updateSize(16 + 60 - model.value)
                }
                .onEnded { _ in
                  onEnd()
                }
            )
          Spacer()
        }
      }
      .frame(width: gr.size.width, height: gr.size.height)
    }
  }
}
