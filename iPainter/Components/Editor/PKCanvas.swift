//
//  PKCanvas.swift
//  iPainter
//
//  Created by Artem Shapovalov on 22.10.2022.
//

import PencilKit

class PKCanvas: PKCanvasView {
  
  var points: [CGPoint] = []
  var onTouchesBegan: (UITouch?) -> () = {_ in}
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    guard let point = touches.first?.location(in: self) else {
      return
    }
    
    points.append(point)
    onTouchesBegan(touches.first)
  }
  
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    guard let point = touches.first?.location(in: self) else {
      return
    }
    
    points.append(point)
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    
//    points = []
  }
}
