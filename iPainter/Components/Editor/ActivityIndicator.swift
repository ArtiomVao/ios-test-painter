//
//  ActivityIndicator.swift
//  iPainter
//
//  Created by Artem Shapovalov on 29.10.2022.
//

import SwiftUI

struct ActivityIndicator: UIViewRepresentable {
  func makeUIView(context: Context) -> some UIView {
    let spinner = UIActivityIndicatorView(style: .medium)
    spinner.color = .white
    spinner.startAnimating()
    return spinner
  }
  func updateUIView(_ uiView: UIViewType, context: Context) {
  }
}
