//
//  TextEditorViewController.swift
//  iPainter
//
//  Created by Artem Shapovalov on 22.10.2022.
//

import UIKit
import SwiftUI

class TextEditorViewController: UIViewController {
  
  var onDone: () -> () = {}
  
  var textView = UITextView()
  let bgView = UIView()
  
  let headerView = UIView()
  let cancelButton = UIButton()
  let doneButton = UIButton()
  var fontSliderView = UIView()
  
  let keyboardAwareBottomLayoutGuide: UILayoutGuide = UILayoutGuide()
  var keyboardTopAnchorConstraint: NSLayoutConstraint? = nil
  var bgRects: [CGRect] = []
  
  var textStorage = CustomTextStorage()
  var layoutManager = CustomLayoutManager()
  
  private let _justChangingFont: Bool
  private let _defaultText: NSAttributedString?
  private let _colorPicker = UIColorPickerViewController()

  init(justChangingFont: Bool = false, text: NSAttributedString? = nil) {
    _justChangingFont = justChangingFont
    _defaultText = text
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    .lightContent
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.addSubview(bgView)
    
    bgView.backgroundColor = .black
    bgView.alpha = 0.5
    
    // MARK: - Color picker
    
    _colorPicker.modalPresentationStyle = .popover
    _colorPicker.modalTransitionStyle = .coverVertical
    _colorPicker.delegate = self
    
    // MARK: - TextView
    
    textStorage.addLayoutManager(layoutManager)
    let textContainer = NSTextContainer(size: .zero)
    layoutManager.addTextContainer(textContainer)
    layoutManager.delegate = self
    layoutManager.addBgRect = {
      self.bgRects.append($0)
    }
    
    textView = UITextView(frame: .zero, textContainer: textContainer)
    
    view.addSubview(textView)
    
    textView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    textView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
    textView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
    textView.heightAnchor.constraint(equalToConstant: 200).isActive = true
    textView.translatesAutoresizingMaskIntoConstraints = false
    
    textView.becomeFirstResponder()
    
    textView.delegate = self
    textView.backgroundColor = .clear
    textView.autocorrectionType = .no
    
    // MARK: - InputAccessoryView
    
    if let m = ToolsModel.shared.textModel {
      let textControlsView = TextControls(model: m,
                                          colorPicker: true,
                                          blurEffect: true) {
        self._presentColorPicker()
      } onChangeFont: {
        self.textView.font = $0
      } onChangeModel: {
        if $0 {
          return
        }
        self.updateTextView()
      }.frame(height: 50)
      
      let textControls = UIHostingController(rootView: textControlsView).view
      
      textControls?.frame.size.height = 50
      textControls?.backgroundColor = .clear
      
      textView.inputAccessoryView = textControls
    }
    
    // MARK: - Font slider
    
    let fontSlider = FontSlider(model: FontSlider.Model()) {
      if let textModel = ToolsModel.shared.textModel {
        let font = TextModel.font(of: $0, with: textModel.fontDesign.value)
        ToolsModel.shared.textModel?.size = $0
        ToolsModel.shared.textModel?.font = font
        self.textView.font = font
      }
    }
    
    fontSliderView = UIHostingController(rootView: fontSlider).view
    
    view.addSubview(headerView)
    view.addSubview(fontSliderView)
    
    fontSliderView.backgroundColor = .clear
    fontSliderView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    fontSliderView.centerYAnchor.constraint(
      equalTo: view.centerYAnchor, constant: -60).isActive = true
    fontSliderView.heightAnchor.constraint(equalToConstant: 170).isActive = true
    fontSliderView.widthAnchor.constraint(equalToConstant: 40).isActive = true
    fontSliderView.translatesAutoresizingMaskIntoConstraints = false
    
    // MARK: - Header view
    
    headerView.backgroundColor = .black
    
    headerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    headerView.topAnchor.constraint(
      equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
    headerView.heightAnchor.constraint(equalToConstant: 60).isActive = true
    headerView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    headerView.translatesAutoresizingMaskIntoConstraints = false
    
    headerView.addSubview(cancelButton)
    headerView.addSubview(doneButton)
    
    // MARK: - Cancel button
    
    cancelButton.setTitle("Cancel", for: .normal)
    cancelButton.addTarget(self, action: #selector(_cancel), for: .touchDown)
    
    cancelButton.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 10).isActive = true
    cancelButton.centerYAnchor.constraint(
      equalTo: headerView.centerYAnchor).isActive = true
    cancelButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
    cancelButton.widthAnchor.constraint(equalToConstant: 70).isActive = true
    cancelButton.translatesAutoresizingMaskIntoConstraints = false
    
    // MARK: - Done button
    
    doneButton.setTitle("Done", for: .normal)
    doneButton.addTarget(self, action: #selector(_done), for: .touchDown)
    
    doneButton.rightAnchor.constraint(equalTo: headerView.rightAnchor, constant: -10).isActive = true
    doneButton.centerYAnchor.constraint(
      equalTo: headerView.centerYAnchor).isActive = true
    doneButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
    doneButton.widthAnchor.constraint(equalToConstant: 70).isActive = true
    doneButton.translatesAutoresizingMaskIntoConstraints = false
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    
    bgView.frame = view.frame
  }
  
  // MARK: - Navigation
  
  @objc private func _cancel() {
    ToolsModel.shared.reset()
    dismiss(animated: true)
  }
  
  @objc private func _done() {
    debugPrint(bgRects.count)
    bgRects = []
    updateTextView()
    debugPrint(bgRects)
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
      debugPrint(self.bgRects)
      self.onDone()
      self.dismiss(animated: true)
    }
  }
  
  func updateTextView() {
    guard let model = ToolsModel.shared.textModel else {
      return
    }
    
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.alignment = model.alignment.value
    
    debugPrint(model.font.familyName)
    
    var attributes: [NSAttributedString.Key: Any] = [
      .paragraphStyle: paragraphStyle,
      .foregroundColor: model.color,
      .font: model.font]
    
    switch model.bgStyle {
    case .fill:
      attributes[.underlineStyle] = NSUnderlineStyle.single.rawValue
      attributes[.underlineColor] = UIColor.black
    case .semi:
      attributes[.underlineStyle] = NSUnderlineStyle.single.rawValue
      attributes[.underlineColor] = UIColor(white: 1, alpha: 0.5)
    case .stroke:
      attributes[.strokeColor] = UIColor.black
      attributes[.strokeWidth] = -4
    case .default_:
      break
    }
    
    textView.textStorage.beginEditing()

    textView.typingAttributes = attributes
    textView.textAlignment = model.alignment.value
    if let length = textView.attributedText?.length {
      textView.textStorage.setAttributes(
        attributes, range: NSMakeRange(0, length)
      )
    }
    
    textView.textStorage.endEditing()
  }
  
  func prepareTextView() {
    guard let model = ToolsModel.shared.textModel else {
      return
    }
    
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.alignment = model.alignment.value
    
    textView.typingAttributes = [
      .foregroundColor: model.color,
      .paragraphStyle: paragraphStyle,
      .font: model.font,
      .underlineStyle: NSUnderlineStyle.single.rawValue,
      .underlineColor: UIColor.black,
    ]
    
    textView.isScrollEnabled = false
    textView.layoutManager.allowsNonContiguousLayout = false
    textView.allowsEditingTextAttributes = true
    
    if let text = _defaultText?.string {
      textView.text = text
      if _justChangingFont {
        textView.endEditing(true)
      }
    }
  }
  
  private func _presentColorPicker() {
    self.present(_colorPicker, animated: true)
  }
}

extension TextEditorViewController: UITextViewDelegate {
  
  func textViewDidBeginEditing(_ textView: UITextView) {
    prepareTextView()
  }
  
  func textViewDidChange(_ textView: UITextView) {
    if textView.text.isEmpty {
      doneButton.alpha = 0.5
      doneButton.isUserInteractionEnabled = false
    } else {
      doneButton.alpha = 1
      doneButton.isUserInteractionEnabled = true
    }
  }
}

extension TextEditorViewController: NSLayoutManagerDelegate {
  
  func layoutManager(_ layoutManager: NSLayoutManager,
                     lineSpacingAfterGlyphAt glyphIndex: Int,
                     withProposedLineFragmentRect rect: CGRect) -> CGFloat {
    return CGFloat(floorf(Float(glyphIndex) / 100.0))
  }
}

extension TextEditorViewController: UIViewControllerTransitioningDelegate {
  
  func presentationController(
    forPresented presented: UIViewController,
    presenting: UIViewController?,
    source: UIViewController
  ) -> UIPresentationController? {
    PresentationController(presentedViewController: presented,
                           presenting: presenting)
  }
}

extension TextEditorViewController: UIColorPickerViewControllerDelegate {
  
  func colorPickerViewController(_ viewController: UIColorPickerViewController,
                                 didSelect color: UIColor,
                                 continuously: Bool)
  {
    ToolsModel.shared.textModel?.color = color
    ToolsModel.shared.objectWillChange.send()
    self.updateTextView()
  }
}
