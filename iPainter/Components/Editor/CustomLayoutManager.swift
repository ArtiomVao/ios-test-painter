//
//  CustomLayoutManager.swift
//  iPainter
//
//  Created by Artem Shapovalov on 23.10.2022.
//

import UIKit
import SwiftUI

class CustomLayoutManager: NSLayoutManager {
  
  var addBgRect: (CGRect) -> () = { _ in }
  
  override func drawUnderline(forGlyphRange glyphRange: NSRange,
                              underlineType underlineVal: NSUnderlineStyle,
                              baselineOffset: CGFloat,
                              lineFragmentRect lineRect: CGRect,
                              lineFragmentGlyphRange lineGlyphRange: NSRange,
                              containerOrigin: CGPoint) {
    guard let textModel = ToolsModel.shared.textModel else {
      return
    }
    
    switch textModel.bgStyle {
    case .fill: UIColor.black.setFill()
    case .semi: UIColor(white: 1, alpha: 0.5).setFill()
    default: return
    }
    
    guard let container = textContainer(forGlyphAt: glyphRange.location,
                                        effectiveRange: nil) else { return }
    
    let boundingRect = boundingRect(forGlyphRange: glyphRange, in: container)
    var offsetRect = boundingRect.offsetBy(dx: containerOrigin.x, dy: containerOrigin.y)
    offsetRect = offsetRect.integral.insetBy(dx: -5, dy: -2)
    addBgRect(offsetRect)
    let path = UIBezierPath(roundedRect: offsetRect, cornerRadius: 5)
    path.fill(with: .copy, alpha: 1)
  }
}

