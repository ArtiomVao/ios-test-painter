//
//  SliderBackgroundShape.swift
//  iPainter
//
//  Created by Artem Shapovalov on 17.10.2022.
//

import SwiftUI
import CoreGraphics

struct SliderBackgroundShape: Shape {
  func path(in rect: CGRect) -> Path {
    Path { path in
      let r1: CGFloat = 12
      let r2: CGFloat = 2
      
      path.move(to: CGPoint(x: rect.maxX - r1, y: rect.midY - r1))
      
      path.addLine(to: CGPoint(x: rect.minX + r2, y: rect.midY - r2))
      
      path.addArc(center: CGPoint(x: rect.minX + r2, y: rect.midY),
                  radius: r2,
                  startAngle: Angle(degrees: 90),
                  endAngle: Angle(degrees: 270),
                  clockwise: false)
      
      path.addLine(to: CGPoint(x: rect.minX + r2, y: rect.midY + r2))
      
      path.addLine(to: CGPoint(x: rect.maxX - r1, y: rect.midY + r1))
      
      path.addArc(center: CGPoint(x: rect.maxX - r1, y: rect.midY),
                  radius: r1,
                  startAngle: Angle(degrees: -90),
                  endAngle: Angle(degrees: 90),
                  clockwise: false)
    }
  }
}

struct SliderBackgroundShape_Previews: PreviewProvider {
  static var previews: some View {
    SliderBackgroundShape().frame(width: 300, height: 50)
  }
}
