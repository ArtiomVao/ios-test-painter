//
//  Tool.swift
//  iPainter
//
//  Created by Artem Shapovalov on 16.10.2022.
//

import SwiftUI
import PencilKit

enum Tool: Int, CaseIterable, Identifiable {
  
  var id: Int { rawValue }
  
  case pen
  case brush
  case neon
  case pencil
  case lasso
  case eraser
  case blurEraser
  case objectEraser
  
  var tip: String? {
    switch self {
    case .pen: return "tip_pen"
    case .brush: return "tip_brush"
    case .neon: return "tip_neon"
    case .pencil: return "tip_pencil"
    case .lasso, .eraser, .blurEraser, .objectEraser: return nil
    }
  }
  
  var base: String {
    switch self {
    case .pen: return "base_pen"
    case .brush: return "base_brush"
    case .neon: return "base_neon"
    case .pencil: return "base_pencil"
    case .lasso: return "base_lasso"
    case .eraser: return "base_eraser"
    case .blurEraser: return "base_blur_eraser"
    case .objectEraser: return "base_object_eraser"
    }
  }
  
  var isDrawingTool: Bool {
    switch self {
    case .pen, .brush, .neon, .pencil: return true
    default: return false
    }
  }
  
  var menuItem: MenuItem {
    switch self {
    case .pen, .brush, .neon, .pencil, .lasso: return .arrow
    case .eraser:       return .eraser
    case .blurEraser:   return .blurEraser
    case .objectEraser: return .objectEraser
    }
  }
  
  var pkTool: PKTool {
    let m = ToolsModel.shared
    let id = min(Self.drawingTools.count - 1, rawValue)
    let c: UIColor
    if UIScreen.main.traitCollection.userInterfaceStyle == .dark {
      c = PKInkingTool.convertColor(m.colors[id], from: .light, to: .dark)
    } else {
      c = m.colors[id]
    }
    let w = m.width[id]
    switch self {
    case .pen: return PKInkingTool(.pen, color: c, width: w)
    case .brush: return PKInkingTool(.marker, color: c, width: w)
    case .neon: return PKInkingTool(.pen, color: c, width: w)
    case .pencil: return PKInkingTool(.pencil, color: c, width: w)
    case .lasso: return PKLassoTool()
    case .eraser: return PKEraserTool(.bitmap)
    case .blurEraser: return PKEraserTool(.bitmap)
    case .objectEraser: return PKEraserTool(.vector)
    }
  }
  
  var view: some View {
    ToolView(tool: self)
  }
  
  var uiView: UIView {
    UIHostingController(rootView: view).view
  }
  
  static func getTools(eraser: Self = .eraser) -> [Self] {
    drawingTools + [.lasso, ToolsModel.shared.currentEraser]
  }
  
  static var drawingTools: [Self] {
    [.pen, .brush, .neon, .pencil]
  }
}

struct ToolView: View {
  
  var tool: Tool
  
  @ObservedObject private var _model = ToolsModel.shared
  
  private var _color: Color {
    Color(_model.colors[tool.rawValue])
  }
  
  private var _widthIndicatorHeight: CGFloat {
    (_model.width[tool.rawValue] / 30) * 100
  }
  
  private var _widthIndicatorOffset: CGFloat {
    let o: CGFloat
    switch tool {
    case .pen:    o = 14
    case .pencil: o = 0
    default:      o = -24
    }
    return o + (_widthIndicatorHeight - 100) / 2
  }
  
  var body: some View {
    ZStack {
      Image(tool.base)
      if tool.isDrawingTool {
        RoundedRectangle(cornerRadius: 6, style: .continuous)
          .fill(_color)
          .frame(width: 102, height: _widthIndicatorHeight)
          .offset(y: _widthIndicatorOffset)
          .opacity(0.5)
      }
      if let tip = tool.tip {
        Image(tip).colorMultiply(_color)
      }
    }
  }
}
