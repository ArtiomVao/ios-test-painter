//
//  TextControls.swift
//  iPainter
//
//  Created by Artem Shapovalov on 23.10.2022.
//

import SwiftUI

struct TextControls: View {
  
  @ObservedObject var model: TextModel
  var colorPicker = false
  var blurEffect = false
  var presentColorPicker: () -> () = {}
  var onChangeFont: (UIFont) -> () = { _ in }
  var onChangeModel: (Bool) -> ()
  
  var body: some View {
    HStack(spacing: 12) {
      if colorPicker {
        ColorPickerButton(action: presentColorPicker)
      }
      
      Button {
        model.selectNextBgStyle()
        onChangeModel(false)
      } label: {
        _image(model.bgStyle.image)
      }
      
      Button {
        model.selectNextAlignment()
        onChangeModel(false)
      } label: {
        _image(model.alignment.image)
      }
      
      ScrollView(.horizontal, showsIndicators: false) {
        HStack(spacing: 10) {
          ForEach(TextModel.FontDesign.allCases, id: \.id) { fd in
            Button {
              let font = TextModel.font(of: model.size, with: fd.value)
              model.fontDesign = fd
              model.font = font
              onChangeFont(font)
              onChangeModel(true)
            } label: {
              Text(fd.name)
                .font(Font(TextModel.font(of: 14, with: fd.value)))
                .foregroundColor(.white)
                .padding(.horizontal, 5)
                .frame(height: 30)
                .background(
                  RoundedRectangle(cornerRadius: 10, style: .continuous)
                    .stroke(.white, lineWidth: 1)
                    .opacity(model.fontDesign == fd ? 1 : 0.5)
                )
            }
          }
        }
        .layoutPriority(10)
        .frame(height: 50)
        .padding(.horizontal)
      }
      .frame(minWidth: 150)
      .layoutPriority(10)
    }
    .padding(.leading, blurEffect ? 12 : 0)
    .background(_blurEffect())
  }
  
  private func _image(_ i: String) -> some View {
    Image(i)
      .renderingMode(.original)
      .resizable()
      .aspectRatio(contentMode: .fit)
      .frame(width: 30, height: 30)
  }
  
  @ViewBuilder
  private func _blurEffect() -> some View {
    if blurEffect {
      VisualEffectView(effect: UIBlurEffect(style: .systemThinMaterial))
    }
  }
}
