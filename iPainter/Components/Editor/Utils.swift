//
//  Utils.swift
//  iPainter
//
//  Created by Artem Shapovalov on 25.10.2022.
//

import Foundation

typealias RotationInfo = (maxX: CGFloat,
                          maxY: CGFloat,
                          minX: CGFloat,
                          minY: CGFloat,
                          deltaX: CGFloat,
                          deltaY: CGFloat,
                          midPoint: CGPoint)

class Utils {
  
  static func getRotation(for points: [CGPoint]) -> RotationInfo {
    let maxX = points.max { $0.x < $1.x }?.x ?? 0
    let maxY = points.max { $0.y < $1.y }?.y ?? 0
    let minX = points.min { $0.x < $1.x }?.x ?? 0
    let minY = points.min { $0.y < $1.y }?.y ?? 0
    
    let deltaX = (maxX - minX) / 2 + minX
    let deltaY = (maxY - minY) / 2 + minY
    
    let midPoint = CGPoint(x: (maxX + minX) / 2, y: (maxY + minY) / 2)
    
    return (maxX: maxX, maxY: maxY, minX: minX, minY: minY,
            deltaX: deltaX, deltaY: deltaY,
            midPoint: midPoint)
  }
}
