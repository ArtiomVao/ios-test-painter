//
//  ToolsModel.swift
//  iPainter
//
//  Created by Artem Shapovalov on 16.10.2022.
//

import SwiftUI

class ToolsModel: ObservableObject {
  
  enum EditorStyle {
    case draw
    case text
    case shape
  }
  
  static var shared = ToolsModel()
  
  @Published var selectedTool = Tool.pen
  @Published var colors: [UIColor]
  @Published var width: [CGFloat]
  @Published var editedTool: Tool? = nil
  @Published var editorStyle: EditorStyle = .draw
  @Published var textModel: TextModel? = nil
  @Published var shapeModel: ShapeModel? = nil
  @Published var editedDrawingId: Int? = nil
  @Published var colorPickerLoc: [CGPoint?]
  @Published var currentEraser: Tool = .eraser
  @Published var currentTip: MenuItem = .round
  @Published var downloading = false
  @Published var edited = false
  @Published var drawingsCount = 0
  
  init() {
    colors = Array<UIColor>.init(repeating: UIColor.white,
                                 count: Tool.drawingTools.count)
    
    width = Array<CGFloat>.init(repeating: 10,
                                count: Tool.allCases.count)
    
    colorPickerLoc = Array<CGPoint?>.init(repeating: nil,
                                          count: Tool.drawingTools.count)
  }
  
  func set(color: UIColor) {
    if colors.count - 1 < selectedTool.rawValue {
      return
    }
    colors[selectedTool.rawValue] = color
  }
  
  func set(width: CGFloat) {
    if self.width.count - 1 < selectedTool.rawValue {
      return
    }
    self.width[selectedTool.rawValue] = width
  }
  
  func set(colorPickerLoc: CGPoint?) {
    if self.colorPickerLoc.count - 1 < selectedTool.rawValue {
      return
    }
    self.colorPickerLoc[selectedTool.rawValue] = colorPickerLoc
  }
  
  func getColor() -> UIColor {
    colors[min(Tool.drawingTools.count - 1, selectedTool.rawValue)]
  }
  
  func getWidth() -> CGFloat {
    width[min(Tool.allCases.count - 1, selectedTool.rawValue)]
  }
  
  func getColorPickerLoc() -> CGPoint? {
    colorPickerLoc[min(Tool.drawingTools.count - 1, selectedTool.rawValue)]
  }
  
  func reset() {
    editorStyle = .draw
    editedDrawingId = nil
    editedTool = nil
    textModel = nil
    shapeModel = nil
  }
  
  func selectEraser(_ tool: Tool) {
    selectedTool = tool
    editedTool = tool
    currentEraser = tool
  }
}
