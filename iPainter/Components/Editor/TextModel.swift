//
//  TextModel.swift
//  iPainter
//
//  Created by Artem Shapovalov on 23.10.2022.
//

import SwiftUI

class TextModel: ObservableObject {
  
  enum BgStyle: Int, CaseIterable, Identifiable {
    
    var id: Int { rawValue }
    
    case default_
    case fill
    case semi
    case stroke
    
    var image: String {
      switch self {
      case .default_: return "a_default"
      case .fill: return "a_filled"
      case .semi: return "a_semi"
      case .stroke: return "a_stroke"
      }
    }
  }
  
  enum Alignment: Int, CaseIterable, Identifiable {
    var id: Int { rawValue }
    
    case center
    case left
    case right
    
    var value: NSTextAlignment {
      switch self {
      case .center: return .center
      case .left: return .left
      case .right: return .right
      }
    }
    
    var image: String {
      switch self {
      case .center: return "text_center"
      case .left: return "text_left"
      case .right: return "text_right"
      }
    }
  }
  
  enum FontDesign: Int, CaseIterable, Identifiable {
    var id: Int { rawValue }
    
    case sf
    case ny
    case mono
    case custom
    
    var value: UIFontDescriptor.SystemDesign {
      switch self {
      case .sf:     return .default
      case .ny:     return .serif
      case .mono:   return .monospaced
      case .custom: return .rounded
      }
    }
    
    var name: String {
      switch self {
      case .sf:     return "San Francisco"
      case .ny:     return "New York"
      case .mono:   return "Monospaced"
      case .custom: return "Custom Font"
      }
    }
  }
  
  @Published var color: UIColor = .white
  @Published var font: UIFont = TextModel.font(of: 26, with: .default)
  @Published var size: CGFloat = 26
  @Published var weight: CGFloat = 10
  @Published var alignment: Alignment = .center
  @Published var bgStyle: BgStyle = .fill
  @Published var fontDesign: FontDesign = .sf
  
  func selectNextBgStyle() {
    switch bgStyle {
    case .default_: bgStyle = .fill
    case .fill:     bgStyle = .semi
    case .semi:     bgStyle = .stroke
    case .stroke:   bgStyle = .default_
    }
  }
  
  func selectNextAlignment() {
    switch alignment {
    case .center: alignment = .left
    case .left:   alignment = .right
    case .right:  alignment = .center
    }
  }
  
  // MARK: - Fonts
  
  static func font(of size: CGFloat,
                   with design: UIFontDescriptor.SystemDesign) -> UIFont {
    let descriptor = UIFont.systemFont(ofSize: size,
                                       weight: .semibold).fontDescriptor
    
    if let d = descriptor.withDesign(design) {
      return UIFont(descriptor: d, size: 0.0)
    }

    return UIFont(descriptor: descriptor, size: 0.0)
  }
}
