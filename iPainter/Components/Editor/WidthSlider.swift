//
//  WidthSlider.swift
//  iPainter
//
//  Created by Artem Shapovalov on 17.10.2022.
//

import SwiftUI

struct WidthSlider: View {
  
  
  class Model: ObservableObject {
    
    @Published var value: CGFloat
    
    init() {
      if let shapeModel = ToolsModel.shared.shapeModel {
        self.value = shapeModel.width
        return
      }
      if let textModel = ToolsModel.shared.textModel {
        self.value = textModel.size
        return
      }
      self.value = ToolsModel.shared.getWidth()
    }
  }
  
  @ObservedObject var model: Model
  var sliderRange: ClosedRange<Double> = 3...30
  var updateWidth: (CGFloat) -> ()
  
  @State private var _lastCoordinateValue: CGFloat = 0.0
  
  var body: some View {
    GeometryReader { gr in
      let thumbSize = gr.size.height * 0.8
      let minValue = gr.size.width * 0.015
      let maxValue = (gr.size.width * 0.98) - thumbSize
      
      let scaleFactor = (maxValue - minValue) / (sliderRange.upperBound - sliderRange.lowerBound)
      let lower = sliderRange.lowerBound
      let sliderVal = (model.value - lower) * scaleFactor + minValue
      
      ZStack {
        SliderBackgroundShape()
          .fill(.gray)
          .frame(height: 30)
          .padding(.horizontal, 5)
          .opacity(0.5)
        HStack {
          Circle()
            .foregroundColor(.white)
            .frame(width: thumbSize, height: thumbSize)
            .offset(x: sliderVal)
            .shadow(radius: 10)
            .gesture(
              DragGesture(minimumDistance: 0)
                .onChanged { v in
                  if (abs(v.translation.width) < 0.1) {
                    _lastCoordinateValue = sliderVal
                  }
                  if v.translation.width > 0 {
                    let nextCoordinateValue = min(
                      maxValue, _lastCoordinateValue + v.translation.width
                    )
                    model.value = ((nextCoordinateValue - minValue) / scaleFactor)  + lower
                  } else {
                    let nextCoordinateValue = max(
                      minValue, _lastCoordinateValue + v.translation.width
                    )
                    model.value = ((nextCoordinateValue - minValue) / scaleFactor) + lower
                  }
                  
                  updateWidth(model.value)
                }
            )
          Spacer()
        }
      }
    }
  }
}
