//
//  CGCanvasView.swift
//  iPainter
//
//  Created by Artem Shapovalov on 20.10.2022.
//

import UIKit

class CGCanvasView: UIView {
  
  struct Drawing {
    var line:  Line?
    var image: UIImage?
    var imageOffset: CGPoint
    var text: NSMutableAttributedString?
    var frame: CGRect?
    var textBg: [CGRect]?
    var textModel: TextModel? = nil
    var shapeModel: ShapeModel? = nil
    var rotation: CGFloat? = nil
    var scale: CGFloat? = nil
    var colorPickerLocation: CGPoint? = nil
    
    init(l: Line? = nil,
         i: UIImage? = nil,
         t: NSMutableAttributedString? = nil,
         f: CGRect? = nil,
         tBg: [CGRect]? = nil) {
      line = l
      image = i
      imageOffset = .zero
      text = t
      frame = f
      textBg = tBg
    }
  }
  
  struct Line {
    var strokeWidth: CGFloat
    var color: UIColor
    var points: [CGPoint]
//    var bgImage: UIImage? = nil
    
    var shadowBlur: CGFloat     = 0
    var blendMode:  CGBlendMode = .normal
    var arrow: [CGPoint]? = nil
    var arrowColor: CGColor? = nil
  }
  
  var onTouchesBegan: (UITouch?) -> () = {_ in}
  var onTouchesEnded: () -> () = {}
  
  private var _canvasRect: CGRect = .zero
  
  var drawings: [Drawing] = [] {
    didSet {
      ToolsModel.shared.drawingsCount = drawings.count
    }
  }
  var selectedDrawings: [Int] = []
  var draggedDrawings: [Int] = []
  
  func undo() {
    _ = drawings.popLast()
    deselectAll()
  }
  
  func clear() {
    drawings.removeAll()
    deselectAll()
  }
  
  func deselectAll() {
    selectedDrawings = []
    setNeedsDisplay()
  }
  
  func selectDrawing(_ i: Int) -> Bool {
    if selectedDrawings.contains(i) {
      return true
    }
    deselectAll()
    selectedDrawings.append(i)
    setNeedsDisplay()
    return false
  }
  
  func selectDrawingAppend(_ i: Int) {
    if selectedDrawings.contains(i) {
      return
    }
    selectedDrawings.append(i)
    setNeedsDisplay()
  }
  
  // MARK: - Delete
  
  func deleteSelectedDrawings() {
    selectedDrawings.forEach {
      drawings.remove(at: $0)
    }
    
    let d = drawings.first { $0.line?.blendMode != .clear }
    
    if d == nil {
      clear()
    }
    
    deselectAll()
    setNeedsDisplay()
  }
  
  func delete(at i: Int) {
    drawings.remove(at: i)
    setNeedsDisplay()
  }
  
  func moveSelectedDrawingForward() {
    var removedDrawings: [Drawing] = []
    
    selectedDrawings.forEach {
      removedDrawings.append(drawings.remove(at: $0))
    }
    
    removedDrawings.forEach {
      drawings.append($0)
    }
    deselectAll()
    setNeedsDisplay()
  }
  
  func addToDragged(_ i: Int) {
    if draggedDrawings.contains(i) {
      return
    }
    
    draggedDrawings.append(i)
  }
  
  // MARK: - Add drawings
  
  func add(image: UIImage, points: [CGPoint]) {
    let line = Line(strokeWidth: ToolsModel.shared.getWidth(),
                    color: .clear,
                    points: points)
    drawings.append(Drawing(l: line, i: image))
    _setArrow()
    setNeedsDisplay()
  }
  
  func add(text: NSMutableAttributedString, in frame: CGRect, bg: [CGRect]) {
    deselectAll()
    var f = frame
    f.origin.x = bounds.midX - frame.width / 2
    f.origin.y = bounds.midY - frame.height / 2
    var d = Drawing(t: text, f: f, tBg: bg)
    d.textModel = ToolsModel.shared.textModel
    drawings.append(d)
    _ = selectDrawing(drawings.count - 1)
    setNeedsDisplay()
  }
  
  func add(shape: MenuItem, shapeModel: ShapeModel) {
    deselectAll()
    var points: [CGPoint] = []

    switch shape {
    case .rectangle:
      let size = CGSize(width: 120, height: 120)
      let minX = bounds.midX - size.width / 2
      let maxX = bounds.midX + size.width / 2
      let minY = bounds.midY - size.height / 2
      let maxY = bounds.midY + size.height / 2
      var x = minX
      var y = minY
      let pointsInLine: CGFloat = 10
      let stepX = size.width / pointsInLine
      let stepY = size.height / pointsInLine
      while x <= maxX {
        points.append(CGPoint(x: x, y: y))
        x += stepX
      }
      while y <= maxY {
        points.append(CGPoint(x: x, y: y))
        y += stepY
      }
      while x > minX {
        points.append(CGPoint(x: x, y: y))
        x = max(minX, x - stepX)
      }
      while y > minY {
        points.append(CGPoint(x: x, y: y))
        y = max(minY, y - stepY)
      }
      points.append(CGPoint(x: x, y: y))
    case .ellipse:
      // x(2) / b(2) + y(2) / a(2) = 1
      // y(2) =
      // y = sqrt((1 - x(2)/b(2)) * a(2))
      
      let pointsCount: CGFloat = 25
      let a: CGFloat = 75
      let b: CGFloat = 75
      let step = (b * 2) / pointsCount
      var x: CGFloat = -b
      var y = CGFloat.getYForEllipse(x: x, a: a, b: b)
      points.append(CGPoint(x: x + bounds.midX, y: y + bounds.midY))
      
      while x <= b {
        x += step
        y = CGFloat.getYForEllipse(x: x, a: a, b: b)
        points.append(CGPoint(x: x + bounds.midX, y: y + bounds.midY))
      }
      while x >= -b {
        x -= step
        y = CGFloat.getYForEllipse(x: x, a: a, b: b)
        points.append(CGPoint(x: x + bounds.midX, y: -y + bounds.midY))
      }
    case .bubble:
      let size = CGSize(width: 150, height: 100)
      let minX = bounds.midX - size.width / 2
      let maxX = bounds.midX + size.width / 2
      let minY = bounds.midY - size.height / 2
      let maxY = bounds.midY + size.height / 2
      var x = minX
      var y = minY
      let pointsInLine: CGFloat = 10
      let stepX = size.width / pointsInLine
      let stepY = size.height / pointsInLine
      while x <= maxX {
        points.append(CGPoint(x: x, y: y))
        x += stepX
      }
      while y <= maxY {
        points.append(CGPoint(x: x, y: y))
        y += stepY
      }
      while x >= minX + stepX * 4 {
        points.append(CGPoint(x: x, y: y))
        x -= stepX
      }
      
      x -= stepX * 2
      points.append(CGPoint(x: x, y: y + stepY * 2))
      points.append(CGPoint(x: x, y: y))
      
      while x > minX {
        points.append(CGPoint(x: x, y: y))
        x = max(minX, x - stepX)
      }
      while y > minY {
        points.append(CGPoint(x: x, y: y))
        y = max(minY, y - stepY)
      }
      points.append(CGPoint(x: x, y: y))
    case .star:
      let size = CGSize(width: 160, height: 148)
      let halfX = size.width / 2
      let halfY1 = size.height * 0.54
      let halfY2 = size.height - halfY1
      
      var x = bounds.midX - halfX * 5 / 8
      var y = bounds.midY + halfY2
      points.append(CGPoint(x: x, y: y))
      
      x = bounds.midX - halfX * 3 / 8
      y = bounds.midY + halfY2 * 10 / 68
      points.append(CGPoint(x: x, y: y))
      
      x = bounds.midX - halfX
      y = bounds.midY - halfY1 * 26 / 80
      points.append(CGPoint(x: x, y: y))
      
      x = bounds.midX - halfX * 18 / 80
      points.append(CGPoint(x: x, y: y))
      
      x = bounds.midX
      y = bounds.midY - halfY1
      points.append(CGPoint(x: x, y: y))
      
      x = bounds.midX + halfX * 18 / 80
      y = bounds.midY - halfY1 * 26 / 80
      points.append(CGPoint(x: x, y: y))
      
      x = bounds.midX + halfX
      points.append(CGPoint(x: x, y: y))
      
      x = bounds.midX + halfX * 3 / 8
      y = bounds.midY + halfY2 * 10 / 68
      points.append(CGPoint(x: x, y: y))
      
      x = bounds.midX + halfX * 5 / 8
      y = bounds.midY + halfY2
      points.append(CGPoint(x: x, y: y))
      
      x = bounds.midX
      y = bounds.midY + halfY2 * 32 / 68
      points.append(CGPoint(x: x, y: y))
      
      x = bounds.midX - halfX * 5 / 8
      y = bounds.midY + halfY2
      points.append(CGPoint(x: x, y: y))
    case .arrow:
      let size = CGSize(width: 220, height: 30)
      
      var x = bounds.midX - size.width / 2
      var y = bounds.midY
      points.append(CGPoint(x: x, y: y))
      
      x = bounds.midX + size.width * 0.3
      points.append(CGPoint(x: x, y: y))
      
      y = bounds.midY - size.height / 2
      points.append(CGPoint(x: x, y: y))
      
      x = bounds.midX + size.width / 2
      y = bounds.midY
      points.append(CGPoint(x: x, y: y))
      
      x = bounds.midX + size.width * 0.3
      y = bounds.midY + size.height / 2
      points.append(CGPoint(x: x, y: y))
      
      y = bounds.midY
      points.append(CGPoint(x: x, y: y))
    default:
      break
    }
    
    let line = Line(strokeWidth: shapeModel.width,
                    color: shapeModel.color,
                    points: points)
    var d = Drawing(l: line)
    d.shapeModel = shapeModel
    drawings.append(d)
    
    _ = selectDrawing(drawings.count - 1)
    setNeedsDisplay()
  }
  
  // MARK: - Drawing
  
  override func draw(_ rect: CGRect) {
    super.draw(rect)
    
    _canvasRect = rect
    
    guard let context = UIGraphicsGetCurrentContext() else { return }

    context.clear(rect)
    
    drawings.enumerated().forEach { i, d in
      // MARK: - Line
      _drawLine(for: d, i: i, in: context)
      // MARK: - Text
      _drawText(for: d, i: i, in: context)
    }
  }
  
  // MARK: - Draw line
  
  private func _drawLine(for d: Drawing, i: Int, in context: CGContext) {
    guard let line = d.line else {
      return
    }
    
    context.saveGState()
    context.setLineCap(.round)
    context.setLineJoin(.round)
    context.setFlatness(10)
    context.setShouldAntialias(false)
    
    var ri: RotationInfo = Utils.getRotation(for: [])
    
    // MARK: - Flip
    
    let flipped = d.shapeModel?.flipped == true
    
    if flipped {
      let flip = CGAffineTransform(scaleX: -1, y: 1)
      context.concatenate(flip)
      context.translateBy(x: -bounds.width, y: 0)
      if let p = d.line?.points, d.rotation == nil {
        ri = Utils.getRotation(for: p)
        context.translateBy(x: bounds.width - ri.midPoint.x, y: ri.midPoint.y)
      }
    }
    
    // MARK: - Rotation
    
    if let rotation = d.rotation, let p = d.line?.points {
      ri = Utils.getRotation(for: p)
      
      if flipped {
        context.translateBy(x: bounds.width - ri.midPoint.x, y: ri.midPoint.y)
      } else {
        context.translateBy(x: ri.midPoint.x, y: ri.midPoint.y)
        
      }
      context.rotate(by: rotation)
    }
    
    // MARK: - Scale
    
    if let scale = d.scale {
      if let p = d.line?.points, d.rotation == nil {
        ri = Utils.getRotation(for: p)
        context.translateBy(x: ri.midPoint.x, y: ri.midPoint.y)
      }
      context.scaleBy(x: scale, y: scale)
    }
    
    let filled = d.shapeModel?.filled == true
    
    _drawBorder(for: line, id: i, in: context, ri.deltaX, ri.deltaY)
    _drawPath(for: line, in: context, ri.deltaX, ri.deltaY, filled)
    
    // MARK: - Image
    if let img = d.image {
      let offset = CGPoint(x: d.imageOffset.x - ri.deltaX,
                           y: d.imageOffset.y - ri.deltaY)
      let bnds = CGRect(origin: offset, size: bounds.size)
      img.draw(in: bnds)
    }
    
    context.restoreGState()
  }
  
  private func _drawPath(for line: Line,
                         in context: CGContext,
                         _ deltaX: CGFloat = 0,
                         _ deltaY: CGFloat = 0,
                         _ filled: Bool = false) {
    context.setBlendMode(line.blendMode)
    context.setStrokeColor(line.color.cgColor)
    
    context.setShadow(offset: .zero,
                      blur: line.shadowBlur,
                      color: line.color.cgColor)
    
    context.setLineWidth(CGFloat(line.strokeWidth))
    
    for (i, p) in line.points.enumerated() {
      let point = CGPoint(x: p.x - deltaX, y: p.y - deltaY)
      if i == 0 {
        context.move(to: point)
      } else {
        context.addLine(to: point)
      }
    }
    
    // Fill
    
    if filled {
      context.setFillColor(line.color.cgColor)
      context.drawPath(using: .fillStroke)
    }
    
    context.strokePath()
    
    // Arrow
    
    if let arrow = line.arrow,
        var p1 = arrow.first, var p2 = arrow.last,
        var p0 = line.points.last {
      if let ac = line.arrowColor {
        context.setStrokeColor(ac)
        context.setLineWidth(CGFloat(line.strokeWidth * 0.6))
      }
      
      p0.x -= deltaX
      p0.y -= deltaY
      p1.x -= deltaX
      p1.y -= deltaY
      p2.x -= deltaX
      p2.y -= deltaY
      
      context.move(to: p0)
      context.addLine(to: p1)
      context.move(to: p0)
      context.addLine(to: p2)
    }
    
    context.strokePath()
  }
  
  private func _drawBorder(for line:
                           Line, id: Int,
                           in context: CGContext,
                           _ deltaX: CGFloat = 0,
                           _ deltaY: CGFloat = 0) {
    guard selectedDrawings.contains(id) else {
      return
    }
    
    let borderColor: UIColor = line.color == .systemBlue ? .gray : .systemBlue
    context.setStrokeColor(borderColor.cgColor)
    context.setLineWidth(line.strokeWidth + 5)
    
    for (i, p) in line.points.enumerated() {
      let point = CGPoint(x: p.x - deltaX, y: p.y - deltaY)
      if i == 0 {
        context.move(to: point)
      } else {
        context.addLine(to: point)
      }
    }
    
    context.strokePath()
  }
  
  // MARK: - Draw text
  
  private func _drawText(for d: Drawing, i: Int, in context: CGContext) {
    if let text = d.text, var f = d.frame {
      var deltaX: CGFloat = 0
      var deltaY: CGFloat = 0
      if let r = d.rotation {
        context.saveGState()
        deltaX = f.width / 2 + f.minX
        deltaY = f.height / 2 + f.minY
        context.translateBy(x: f.midX, y: f.midY)
        context.rotate(by: r)
      }
      if let bgs = d.textBg, let m = d.textModel {
        context.saveGState()
        switch m.bgStyle {
        case .default_: break
        case .fill, .semi:
          if m.bgStyle == .fill {
            context.setFillColor(UIColor.black.cgColor)
          } else {
            context.setFillColor(UIColor(white: 1, alpha: 0.5).cgColor)
            context.setBlendMode(.copy)
          }
          for bg in bgs {
            var rect = bg.offsetBy(dx: 0, dy: f.origin.y - 7)
            switch m.alignment {
            case .center: rect.origin.x = f.midX - rect.width / 2
            case .left:   rect.origin.x = f.origin.x - 5
            case .right:  rect.origin.x = f.maxX - rect.width + 5
            }
            
            rect.origin.x -= deltaX
            rect.origin.y -= deltaY
            
            let path = UIBezierPath(roundedRect: rect, cornerRadius: 5).cgPath
            context.addPath(path)
            context.drawPath(using: .fill)
          }
          context.restoreGState()
        case .stroke:
          break
        }
      }
      
      let borderRect = CGRect(x: f.origin.x - 5 - deltaX,
                              y: f.origin.y - 5 - deltaY,
                              width: f.width + 10,
                              height: f.height + 13)
      _drawRectBorder(for: borderRect, id: i, in: context)
      f.origin.x -= deltaX
      f.origin.y -= deltaY
      text.draw(in: f)
      
      if let _ = d.rotation {
        context.restoreGState()
      }
    }
  }
  
  private func _drawRectBorder(for rect: CGRect, id: Int, in context: CGContext) {
    guard selectedDrawings.contains(id) else {
      return
    }
    let clipPath: CGPath = UIBezierPath(roundedRect: rect, cornerRadius: 10).cgPath
    context.saveGState()
    context.addPath(clipPath)
    context.setStrokeColor(UIColor.white.cgColor)
    context.setLineWidth(1)
    context.setLineDash(phase: 0, lengths: [7, 4])
    context.drawPath(using: .stroke)
    context.restoreGState()
  }
  
  // MARK: - Touches
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    var d = Drawing()
    d.line = Line.init(strokeWidth: ToolsModel.shared.getWidth(),
                       color: ToolsModel.shared.getColor(),
                       points: [])
    
    switch ToolsModel.shared.selectedTool {
    case .neon:
      d.line?.shadowBlur = 13
    case .eraser:
      d.line?.blendMode = .clear
      d.line?.color     = .clear
    case .blurEraser:
      break
//      UIGraphicsBeginImageContext(bounds.size)
//      if let context = UIGraphicsGetCurrentContext() {
//        d.line?.blendMode = .clear
//        if let cgImage = context.makeImage() {
//          d.line?.bgImage = cgImage.returnBlurImage()
//        }
//      }
//      UIGraphicsEndImageContext()
    case .lasso:
      d.line?.color = .systemBlue
    default:
      break
    }
    
    drawings.append(d)
    
    onTouchesBegan(touches.first)
  }
  
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    guard let point    = touches.first?.location(in: self),
          var lastD    = drawings.popLast(),
          var lastLine = lastD.line else {
      return
    }
    
    lastLine.points.append(point)
    lastD.line = lastLine
    
    drawings.append(lastD)
    setNeedsDisplay()
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    _setArrow()
    onTouchesEnded()
  }
  
  private func _setArrow() {
    guard ToolsModel.shared.currentTip == .arrowTip  else {
      return
    }
    switch ToolsModel.shared.selectedTool {
    case .pen, .brush, .neon, .pencil:
      if let points = drawings.last?.line?.points {
        var uPoints: [CGPoint] = []
        
        for p in points {
          let res = !uPoints.contains(where: {
            let dx = p.x - $0.x
            let dy = p.y - $0.y
            let d: CGFloat = 15
            return dx > -d && dx < d || dy > -d && dy < -d
          })
          
          if res {
            uPoints.append(p)
          }
        }
        
        if let p1 = uPoints.popLast(), let p2 = uPoints.popLast() {
          let len: CGFloat = 40
          var ap1: CGPoint = .zero
          var ap2: CGPoint = .zero
          let distance = p1.distance(to: p2)
          let xdis = p1.x - p2.x
          let ydis = p1.y - p2.y
          var triCos = sqrt(xdis * xdis) / distance
          var triSin = sqrt(ydis * ydis) / distance
          
          if (p1.x < p2.x && p1.y < p2.y) || (p1.x > p2.y && p1.y > p2.y) {
            let aco = acos(triCos) * 180 / Double.pi
            let newAngle = (180 - aco) * Double.pi / 180
            triCos = -cos(newAngle)
            triSin = -sin(newAngle)
          }
          
          ap1.x = p1.x > p2.x ? -len : len
          ap1.y = p1.y > p2.y ? -20 : 20
          ap2.x = p1.x > p2.x ? -len : len
          ap2.y = p1.y > p2.y ? 20 : -20
          
          let res1 = CGPoint(x: ap1.x * triCos + ap1.y * triSin + p1.x,
                             y: -ap1.x * triSin + ap1.y * triCos + p1.y)
          let res2 = CGPoint(x: ap2.x * triCos + ap2.y * triSin + p1.x,
                             y: -ap2.x * triSin + ap2.y * triCos + p1.y)
          
          drawings[drawings.count - 1].line?.arrow = [res1, res2]
          
          let arrowColor: CGColor
          switch ToolsModel.shared.selectedTool {
          case .brush, .pencil:
            arrowColor = ToolsModel.shared
              .getColor()
              .withAlphaComponent(0.5)
              .cgColor
          default:
            arrowColor = ToolsModel.shared.getColor().cgColor
          }
          
          drawings[drawings.count - 1].line?.arrowColor = arrowColor
          
          setNeedsDisplay()
        }
      }
    default: break
    }
  }
  
  func getPhoto(background: UIImage? = nil) -> UIImage? {
    UIGraphicsBeginImageContext(_canvasRect.size)
    defer {
      UIGraphicsEndImageContext()
    }
    
    if let bg = background {
      let line = Line(strokeWidth: 3, color: .clear, points: [.zero])
      drawings.insert(Drawing(l: line, i: bg), at: 0)
    }
    
    draw(_canvasRect)
    
    return UIGraphicsGetImageFromCurrentImageContext()
  }
}
