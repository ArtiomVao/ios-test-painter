//
//  GestureView.swift
//  iPainter
//
//  Created by Artem Shapovalov on 26.10.2022.
//

import UIKit

class GestureView: UIView {
  
  var points: [CGPoint] = []
  var onTouchesEnded: () -> () = {}
  
  // MARK: - Drawing
  
  override func draw(_ rect: CGRect) {
    super.draw(rect)
    
    guard let context = UIGraphicsGetCurrentContext() else { return }

    context.clear(rect)
    
    context.setLineCap(.round)
    context.setLineJoin(.round)
    context.setFlatness(10)
    context.setShouldAntialias(false)
    context.setStrokeColor(UIColor.systemBlue.cgColor)
    context.setLineWidth(3)
    
    points.enumerated().forEach { i, p in
      if i == 0 {
        context.move(to: p)
      } else {
        context.addLine(to: p)
      }
    }
    
    context.strokePath()
  }
  
  
  // MARK: - Touches
  
//  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//    points = []
//  }
//
//  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//    guard let point    = touches.first?.location(in: self) else {
//      return
//    }
//
//    debugPrint(point)
//
//    points.append(point)
//    setNeedsDisplay()
//  }
//
//  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//    onTouchesEnded()
//    points = []
//    setNeedsDisplay()
//  }
}
