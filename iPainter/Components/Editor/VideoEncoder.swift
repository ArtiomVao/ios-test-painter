//
//  VideoEncoder.swift
//  iPainter
//
//  Created by Artem Shapovalov on 28.10.2022.
//

import UIKit
import CoreVideo
import AVFoundation
import Photos

class VideoEncoder: NSObject {
  
  private var _assetReader: AVAssetReader? = nil
  private var _videoOutput: AVAssetReaderTrackOutput? = nil
  private var _audioOutput: AVAssetReaderTrackOutput? = nil
  private var _videoTrack:  AVAssetTrack? = nil
  private var _audioTrack:  AVAssetTrack? = nil
  
  private var _assetWriter: AVAssetWriter? = nil
  private var _videoInput:  AVAssetWriterInput? = nil
  private var _audioInput:  AVAssetWriterInput? = nil
  
  private var _rendering = false
  private var _writingVideo = true
  private var _writingAudio = true
  private var _tempurl: URL? = nil
  private var _pixelAdapter: AVAssetWriterInputPixelBufferAdaptor? = nil
  private var frameDuration: CMTime = CMTime(value: 1, timescale: 30)
  private var nextPTS: CMTime = .zero
  private var formatDescription: CMFormatDescription? = nil
  private let _videoInputQueue = DispatchQueue(label: "videoQueue")
  private let _audioInputQueue = DispatchQueue(label: "audioQueue")
  private var _cgContext: CGContext? = nil
  private var _videoInfo: (orientation: UIImage.Orientation, isPortrait: Bool) = (.up, false)
  private var _videoFixedSize: CGSize = .zero
  private var _imgSize: CGSize = .zero
  private lazy var compositionURL = FileManager.default.urls(
    for: .documentDirectory,
    in: .userDomainMask
  ).last?.appendingPathComponent("composition.mov")
  
  var filePath = ""
  var image: UIImage? = nil
  var onSaved: () -> () = {}
  var isSidesSwaped = false

  func startReadingAsset(imgSize: CGSize) {
    let url = URL(fileURLWithPath: filePath)
    let asset = AVAsset(url: url)
    _imgSize = imgSize
    _videoInfo.isPortrait = imgSize.width < imgSize.height
    
    do {
      _assetReader = try AVAssetReader(asset: asset)
    } catch {
      fatalError("Unable to read Asset: \(error).")
    }
    
    _videoTrack = asset.tracks(withMediaType: .video).first
    _audioTrack = asset.tracks(withMediaType: .audio).first
    
    guard let _videoTrack else { return }
    
    var size: CGSize = _videoTrack.naturalSize
    isSidesSwaped = (_videoInfo.isPortrait && size.width > size.height)
    || (!_videoInfo.isPortrait && size.width < size.height)
    
    if isSidesSwaped {
      let temp = size.width
      size.width = size.height
      size.height = temp
    }
    
    _videoFixedSize = size
    
    let videoReaderSettings: [String: Any] = [
      kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32ARGB,
    ]
    
    _videoOutput = AVAssetReaderTrackOutput(
      track: _videoTrack, outputSettings: videoReaderSettings
    )
    
    if let _audioTrack {
      _audioOutput = AVAssetReaderTrackOutput(
        track: _audioTrack, outputSettings: nil
      )
    } else {
      _writingAudio = false
    }
    
    if let _videoOutput {
      _assetReader?.add(_videoOutput)
    }
    
    if let _audioOutput {
      _assetReader?.add(_audioOutput)
    }
    
    _assetReader?.startReading()
    
    _createAssetWriter(imgSize: imgSize)
    
    let cgImage = _transformImage()
    
    _videoInput?.requestMediaDataWhenReady(on: _videoInputQueue) {
      while self._writingVideo {
        if self._videoInput?.isReadyForMoreMediaData == false {
          continue
        }
        
        if let buf = self._videoOutput?.copyNextSampleBuffer() {
          self._renderFrame(buf: buf, image: cgImage)
        } else {
          self._videoInput?.markAsFinished()
          self._writingVideo = false
          self._closeWriter()
          break
        }
      }
    }
    
    _audioInput?.requestMediaDataWhenReady(on: _audioInputQueue) {
      while self._writingAudio {
        if self._audioInput?.isReadyForMoreMediaData == false {
          continue
        }
        
        if let buf = self._audioOutput?.copyNextSampleBuffer() {
          self._audioInput?.append(buf)
        } else {
          self._audioInput?.markAsFinished()
          self._writingAudio = false
          self._closeWriter()
          break
        }
      }
    }
  }
  
  private func _createTempFile() {
    guard let curl = FileManager.default.containerURL else {
      return
    }
    do {
      try FileManager.default.createDirectory(
        atPath: curl.appendingPathComponent("caches").path,
        withIntermediateDirectories: true,
        attributes: nil
      )
    } catch {
      debugPrint("Ooops! Something went wrong: \(error)")
    }
    _tempurl = FileManager.default.containerURL?
      .appendingPathComponent("caches")
      .appendingPathComponent("my_temp.mov")
    
    guard let temp = _tempurl else {
      return
    }
    
    debugPrint(FileManager.default.loadAll(path: "caches"))
    
    try? FileManager.default.removeItem(at: temp)
    
    debugPrint(temp.relativePath)
    debugPrint(FileManager.default.loadAll())
    debugPrint(FileManager.default.loadAll(path: "caches"))
  }
  
  private func _createAssetWriter(imgSize: CGSize) {
    _createTempFile()
    guard let _tempurl else { return }
    _assetWriter = try? AVAssetWriter(outputURL: _tempurl, fileType: .mov)
    let scale = UIScreen.main.scale
    let outputSettings: [String: Any] = [
      AVVideoCodecKey: AVVideoCodecType.h264,
      AVVideoWidthKey: NSNumber(value: Float(imgSize.width * scale)),
      AVVideoHeightKey: NSNumber(value: Float(imgSize.height * scale)),
      AVVideoCompressionPropertiesKey: [AVVideoAverageBitRateKey: 3000_000],
    ]
    
    _videoInput = AVAssetWriterInput(mediaType: .video,
                                     outputSettings: outputSettings)
    _audioInput = AVAssetWriterInput(mediaType: .audio,
                                     outputSettings: nil)
    
    guard let _videoInput, let _audioInput else { return }
    
    if let vt = _videoTrack, let at = _audioTrack {
      _videoInput.transform = vt.preferredTransform
      _audioInput.transform = at.preferredTransform
    }
    
    _pixelAdapter = AVAssetWriterInputPixelBufferAdaptor(
      assetWriterInput: _videoInput,
      sourcePixelBufferAttributes: nil
    )
    
    _assetWriter?.add(_videoInput)
    _assetWriter?.add(_audioInput)
    _assetWriter?.startWriting()
    _assetWriter?.startSession(atSourceTime: .zero)
  }
  
  private func _closeWriter() {
    guard !_writingVideo, !_writingAudio else {
      return
    }
    
    _assetWriter?.finishWriting(completionHandler: {
      guard let temp = self._tempurl else { return }
      
      guard self.isSidesSwaped
      else {
        PHPhotoLibrary.shared().performChanges({
          PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: temp)
        }) { saved, error in
          debugPrint(error)
          debugPrint(saved)
          self.onSaved()
        }
        return
      }
      
      let asset = AVAsset(url: temp)
      let mixComposition = AVMutableComposition()

      guard let track = mixComposition.addMutableTrack(
          withMediaType: .video,
          preferredTrackID: Int32(kCMPersistentTrackID_Invalid)
      ) else {
        print("Can't create mix track")
        self.onSaved()
        return
      }
      
      do {
        try track.insertTimeRange(
          CMTimeRangeMake(start: .zero, duration: asset.duration),
          of: asset.tracks(withMediaType: .video)[0],
          at: .zero
        )
      } catch {
        print("Failed to load track")
        self.onSaved()
        return
      }
            
      let audioTrack = mixComposition.addMutableTrack(
        withMediaType: .audio,
        preferredTrackID: 0
      )
      
      do {
        try audioTrack?.insertTimeRange(
          CMTimeRangeMake(
            start: CMTime.zero,
            duration: asset.duration
          ),
          of: asset.tracks(withMediaType: .audio)[0],
          at: .zero
        )
      } catch {
        print("Failed to load Audio track")
      }
      
      let mainInstruction = AVMutableVideoCompositionInstruction()
      mainInstruction.timeRange = CMTimeRangeMake(
        start: .zero,
        duration: asset.duration
      )
      let instruction = self._videoCompositionInstruction(track, asset)
      mainInstruction.layerInstructions = [instruction]
      let mainComposition = AVMutableVideoComposition()
      mainComposition.instructions = [mainInstruction]
      mainComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
      let scale = UIScreen.main.scale
      mainComposition.renderSize = CGSizeMake(self._imgSize.width * scale,
                                              self._imgSize.height * scale)
      
      guard let exporter = AVAssetExportSession(
        asset: mixComposition,
        presetName: AVAssetExportPresetHighestQuality
      )
      else {
        print("Failed to create exporter")
        self.onSaved()
        return
      }
      
      exporter.outputURL = self.compositionURL
      exporter.outputFileType = AVFileType.mov
      exporter.shouldOptimizeForNetworkUse = true
      exporter.videoComposition = mainComposition

      exporter.exportAsynchronously {
        guard let url = self.compositionURL else { return }
        DispatchQueue.main.async {
          self.checkFileSize(sizeUrl: url)
          PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
          }) { saved, error in
            debugPrint(error)
            debugPrint(saved)
            try? FileManager.default.removeItem(at: url)
            self.onSaved()
          }
        }
      } // exporter
    })
  }
  
  private func _transformImage() -> CGImage? {
    guard let uiImage = image,
          var ciImage = CIImage(image: uiImage),
          let track = _videoTrack else {
      return nil
    }
    
    let context = CIContext(options: [
      CIContextOption.workingColorSpace: CGColorSpaceCreateDeviceRGB(),
      CIContextOption.workingFormat: CIFormat.ARGB8 ,
    ])
    
    _orientation(from: track.preferredTransform)
    let extent = ciImage.extent
    var tx: CGAffineTransform
    let scale = _videoFixedSize.width / extent.width
    
    if _videoInfo.isPortrait {
      if isSidesSwaped {
        tx = CGAffineTransform(translationX: extent.minX,
                               y: extent.minY)
        tx = tx.rotated(by: CGFloat.pi / 2)
        tx = tx.translatedBy(x: -extent.minX,
                             y: -extent.height)
        tx = tx.scaledBy(x: scale, y: scale)
        tx = tx.translatedBy(x: 0, y: (extent.height / scale) * (1 - scale) - 1)
      } else {
        tx = CGAffineTransform(scaleX: scale, y: scale)
      }
    } else {
      debugPrint("SCALE: \(scale)")
      tx = CGAffineTransform(scaleX: scale, y: scale)
    }
    
    let ciFilterParams = [
      kCIInputImageKey: ciImage,
      kCIInputTransformKey: NSValue(cgAffineTransform: tx)
    ]
    let ciFilter = CIFilter(name: "CIAffineTransform",
                            parameters: ciFilterParams)
    
    if let transformedImage = ciFilter?.outputImage {
      ciImage = transformedImage
    }
    
    let rect = CGRect(origin: .zero, size: track.naturalSize)
    return context.createCGImage(ciImage, from: rect)
  }
  
  private func _renderFrame(buf: CMSampleBuffer,
                            image cgImage: CGImage?) {
    guard let imgBuf = CMSampleBufferGetImageBuffer(buf), let _videoTrack else {
      return
    }
    
    debugPrint("CVPIXEL: \(CVPixelBufferGetWidth(imgBuf))x\(CVPixelBufferGetHeight(imgBuf))")
    
    if let cgImage {
      CVPixelBufferLockBaseAddress(imgBuf, .readOnly)
      
      let rect = CGRect(origin: .zero, size: _videoTrack.naturalSize)
      
      debugPrint("RECTsize: \(rect.size)")
      
      _cgContext = CGContext(
        data: CVPixelBufferGetBaseAddress(imgBuf),
        width: Int(rect.size.width),
        height: Int(rect.size.height),
        bitsPerComponent: 8,
        bytesPerRow: CVPixelBufferGetBytesPerRow(imgBuf),
        space: CGColorSpaceCreateDeviceRGB(),
        bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue
      )
      
      switch _videoInfo.orientation {
      case .down, .downMirrored:
        debugPrint("DOWN")
        _cgContext?.translateBy(x: rect.midX, y: rect.midY)
        _cgContext?.saveGState()
        _cgContext?.rotate(by: .pi)
        _cgContext?.draw(cgImage, in: CGRectMake(-rect.width / 2,
                                                 -rect.height / 2,
                                                  rect.width,
                                                  rect.height))
        _cgContext?.restoreGState()
      case .up, .upMirrored:
        debugPrint("UP")
        _cgContext?.draw(cgImage, in: rect)
      case .left, .leftMirrored:
        debugPrint("LEFT")
        _cgContext?.draw(cgImage, in: rect)
      case .right, .rightMirrored:
        debugPrint("RIGHT")
        _cgContext?.draw(cgImage, in: rect)
      @unknown default:
        break
      }
      
      CVPixelBufferUnlockBaseAddress(imgBuf, .readOnly)
    }
    
    let pts = CMSampleBufferGetPresentationTimeStamp(buf)
    
    let appended = _pixelAdapter?.append(imgBuf, withPresentationTime: pts)
    
    if appended == false {
      debugPrint(_assetWriter?.error)
    }
  }
  
  private func _copyFile() {
    let sandbox = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last?.appendingPathComponent("temp.mov")

    if let s = sandbox, let _tempurl {
      try? FileManager.default.removeItem(at: s)
      do {
        try FileManager.default.copyItem(at: _tempurl, to: s)
      } catch {
        debugPrint(error)
      }
    }
  }
  
  func checkFileSize(sizeUrl: URL?) {
    guard let url = sizeUrl, let data = NSData(contentsOf: url) else {
      return
    }
    debugPrint(url)
    debugPrint("File size: \(Double(data.length) / 1048576.0) mb")
  }
  
  private func _orientation(from transform: CGAffineTransform) {
    if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
      _videoInfo.orientation = .right
    } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
      _videoInfo.orientation = .left
    } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
      _videoInfo.orientation = .up
    } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
      _videoInfo.orientation = .down
    }
  }
  
  private func _videoCompositionInstruction(
    _ track: AVCompositionTrack,
    _ asset: AVAsset
  ) -> AVMutableVideoCompositionLayerInstruction {
    let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
    let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
//    let scale = UIScreen.main.scale
//    let targetSize = CGSizeMake(_imgSize.width, _imgSize.height)
//    let ratio1 = targetSize.width / targetSize.height
//    let smallHeight = targetSize.width * ratio1
//    let ratio2 = targetSize.height / smallHeight
    let scaleFactor = CGAffineTransform(
      scaleX: 0.59,
      y: 1.78
    )
    instruction.setTransform(
      assetTrack.preferredTransform.concatenating(scaleFactor),
      at: .zero
    )
    return instruction
  }
}
