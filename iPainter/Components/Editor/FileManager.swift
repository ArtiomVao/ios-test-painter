//
//  FileManager.swift
//  iPainter
//
//  Created by Artem Shapovalov on 28.10.2022.
//

import Foundation

extension FileManager {
  
  var containerURL: URL? {
    containerURL(forSecurityApplicationGroupIdentifier: "group.com.artem.iPainer")
  }
  
  func loadAll(path: String = "") -> [String] {
    
    guard let containerURL = containerURL?.appendingPathComponent(path) else {
      return []
    }
    
    var items: [URL] = []
    
    do {
      items = try contentsOfDirectory(
        at: containerURL,
        includingPropertiesForKeys: nil,
        options: .includesDirectoriesPostOrder
      )
    } catch {
      debugPrint("Cant get urls. \(error)")
    }
    
    return items.map { $0.lastPathComponent }
  }
}

