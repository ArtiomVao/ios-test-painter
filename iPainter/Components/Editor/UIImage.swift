//
//  UIImage.swift
//  iPainter
//
//  Created by Artem Shapovalov on 26.10.2022.
//

import UIKit

extension UIImage {
  
  func resize(to targetSize: CGSize) -> UIImage {
    let size = self.size
    
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height
    
    var newSize: CGSize
    if(widthRatio > heightRatio) {
      newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
      newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
    }
    
    let rect = CGRect(origin: .zero, size: newSize)
    
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    self.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage ?? self
  }
}

extension CGImage {
  
  func returnBlurImage() -> UIImage? {
    let beginImage = CIImage(cgImage: self)
    let blurfilter = CIFilter(name: "CIGaussianBlur")
    blurfilter?.setValue(beginImage, forKey: "inputImage")
    
    guard let resultImage = blurfilter?.value(forKey: "outputImage") as? CIImage else {
      return nil
    }
    
    return UIImage(ciImage: resultImage)
  }
}
