//
//  ToolsView.swift
//  iPainter
//
//  Created by Artem Shapovalov on 18.10.2022.
//

import SwiftUI
import PencilKit

struct ToolsView: View {
  
  @ObservedObject private var _model = ToolsModel.shared
  
  var toolPicker: PKToolPicker?
  
  var selectTool: (PKTool) -> ()
  var changeWidth: (CGFloat) -> ()
  var onEditTool: (Bool) -> ()
  var presentColorPicker: () -> ()
  var toggleShapeMenu: () -> ()
  var toggleTipMenu: () -> ()
  var toggleEraserMenu: () -> ()
  var onEditorStyleChange: (ToolsModel.EditorStyle) -> ()
  var onChangeTextModel: (Bool) -> ()
  var onChangeShapeModel: () -> ()
  var download: () -> ()
  var close: () -> ()
  
  @State private var _widthValue: CGFloat = 3
  @State private var _showDiscardChangesAlert = false
  
  private let _editedToolAnimation = Animation.spring(
    response:        0.5,
    dampingFraction: 0.7,
    blendDuration:   0.4
  )
  
  private let _selectedToolAnimation = Animation.spring(
    response:        0.6,
    dampingFraction: 0.45,
    blendDuration:   0.3
  )
  
  private var _alpha: CGFloat {
    _model.editedTool == nil ? 1 : 0
  }
  
  var body: some View {
    VStack(spacing: 10) {
      Spacer()
      _topStack().offset(y: 20)
      _bottomStack()
    }
    .padding(.bottom, 40)
  }
  
  private func _topStack() -> some View {
    HStack {
      VStack {
        Spacer()
        
        ColorPickerButton() {
          presentColorPicker()
        }.opacity(_alpha)
      }
      
      Spacer()
      
      if _model.shapeModel == nil {
        _mainControls()
        
        Spacer()
        
        VStack {
          Spacer()
          
          Button {
            toggleShapeMenu()
          } label: {
            ZStack {
              Circle().fill(Color.gray).opacity(0.15)
              Image("add_shape")
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit)
            }
            .frame(width: 40, height: 40)
          }.opacity(_alpha)
        }
      } else {
        _shapeControls()
      }
    }
  }
  
  private func _bottomStack() -> some View {
    HStack {
      if _model.editedDrawingId != nil || _model.editedTool != nil {
        Button {
          withAnimation(_editedToolAnimation) {
            _model.reset()
            onEditTool(false)
          }
        } label: {
          Image("back")
            .renderingMode(.original)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: 40, height: 40)
        }
      } else {
        Button {
          if _model.drawingsCount > 0 {
            _showDiscardChangesAlert = true
            return
          }
          _model.selectedTool = .pen
          close()
        } label: {
          Image("close")
            .renderingMode(.original)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: 40, height: 40)
        }
        .alert(isPresented: $_showDiscardChangesAlert) {
          Alert(
            title: Text("Discard changes?"),
            primaryButton: .destructive(Text("Discard"), action: {
              _model.selectedTool = .pen
              close()
            }),
            secondaryButton: .cancel(Text("Cancel"))
          )
        }
      }
      
      Spacer()
      
      _bottomControls()
      
      Spacer()
      
      if (_model.editedDrawingId != nil || _model.editedTool != nil) &&
          _model.editorStyle != .text {
        _toolContextMenu()
      } else {
        if _model.downloading {
          ActivityIndicator().frame(width: 40, height: 40)
        } else {
          Button {
            _model.downloading = true
            download()
          } label: {
            Image("download")
              .renderingMode(.original)
              .resizable()
              .aspectRatio(contentMode: .fit)
              .frame(width: 40, height: 40)
          }
          .opacity(_model.drawingsCount > 0 ? 1 : 0.5)
          .disabled(_model.drawingsCount == 0)
        }
      }
    }
    .frame(height: 40)
  }
  
  @ViewBuilder
  private func _toolContextMenu() -> some View {
    if let m = _model.shapeModel {
      HStack {
        Button {
          m.filled = !m.filled
          _model.objectWillChange.send()
          onChangeShapeModel()
        } label: {
          Image(m.filled ? "fill" : "stroke")
            .renderingMode(.original)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: 35, height: 35)
        }
        
        Button {
          m.flipped = !m.flipped
          onChangeShapeModel()
        } label: {
          Image("flip")
            .renderingMode(.original)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: 35, height: 35)
        }
      }
    } else {
      switch _model.selectedTool {
      case .pen, .brush, .neon, .pencil:
        Button {
          toggleTipMenu()
        } label: {
          HStack {
            Text(_model.currentTip.name)
              .font(.system(size: 18))
              .foregroundColor(.white)
            Image(_model.currentTip.icon)
              .renderingMode(.original)
              .resizable()
              .aspectRatio(contentMode: .fit)
              .frame(width: 25, height: 40)
          }
          .frame(width: 100, height: 40)
        }
      case .lasso:
        EmptyView()
      case .eraser, .blurEraser, .objectEraser:
        Button {
          toggleEraserMenu()
        } label: {
          HStack {
            Text(_model.currentEraser.menuItem.name)
              .font(.system(size: 18))
              .foregroundColor(.white)
            Image(_model.currentEraser.menuItem.icon)
              .renderingMode(.original)
              .resizable()
              .aspectRatio(contentMode: .fit)
              .frame(width: 25, height: 40)
          }
          .frame(width: 100, height: 40)
        }
      }
    }
  }
  
  @ViewBuilder
  private func _mainControls() -> some View {
    if let toolPicker {
//      toolPicker
    } else if _model.editorStyle == .draw {
      HStack(spacing: 10) {
        Spacer()
        ForEach(Tool.getTools(), id: \.id) { tool in
          if tool == _model.editedTool || _model.editedTool == nil {
            tool.view
              .scaleEffect(_getToolScale(for: tool))
              .frame(width: 33, height: 100)
              .offset(y: _getToolOffset(for: tool))
              .onTapGesture {
                if tool == _model.selectedTool && tool != .lasso {
                  _toggleEdit(of: tool)
                  return
                }
                withAnimation(_selectedToolAnimation.speed(1.2)) {
                  _model.selectedTool = tool
                  selectTool(tool.pkTool)
                }
              }
          }
        }
        Spacer()
      }
      .overlay(
        Rectangle()
          .fill(Color.black)
          .shadow(color: .black, radius: 10, x: 0, y: -10)
          .offset(y: 80)
      )
      .frame(maxWidth: 200)
    } else if let _ = _model.shapeModel {
      EmptyView()
    } else {
      if let m = _model.textModel {
        TextControls(model: m) {
          onChangeTextModel($0)
        }
        .offset(y: 5)
        .padding(.horizontal, 6)
        .frame(height: 100)
      }
    }
  }
  
  private func _shapeControls() -> some View {
    HStack {
      Text("Colors")
    }
    .frame(width: 33, height: 100)
  }
  
  @ViewBuilder
  private func _bottomControls() -> some View {
    if let _ = _model.editedTool {
      WidthSlider(model: WidthSlider.Model()) {
        _model.set(width: $0)
        changeWidth($0)
      }
      .layoutPriority(10)
      .frame(height: 40)
      .offset(y: 3)
    } else if let _ = _model.shapeModel {
      WidthSlider(model: WidthSlider.Model()) {
        _model.shapeModel?.width = $0
        changeWidth($0)
      }
      .layoutPriority(10)
      .frame(height: 40)
      .offset(y: 3)
    } else {
      GeometryReader { proxy in
        ZStack {
          Capsule().fill(Color.gray).opacity(0.3)
          HStack {
            if _model.editorStyle == .text {
              Spacer()
            }
            Capsule().fill(Color.gray)
              .frame(width: proxy.size.width / 2)
            if _model.editorStyle == .draw {
              Spacer()
            }
          }
          HStack {
            Button {
              _changeEditorStyle(.draw)
            } label: {
              Text("Draw").foregroundColor(.white)
                .frame(width: proxy.size.width / 2)
            }
            Button {
              _changeEditorStyle(.text)
            } label: {
              Text("Text").foregroundColor(.white)
                .frame(width: proxy.size.width / 2)
            }
          }
        }
        .frame(width: proxy.size.width, height: 38)
      }
      .layoutPriority(10)
      .padding(.horizontal, 10)
    }
  }
  
  private func _getToolScale(for tool: Tool) -> CGFloat {
    if tool == _model.editedTool {
      return 0.30
    }
//    return tool == _model.selectedTool ? 0.23 : 0.2
    return 0.2
  }
  
  private func _getToolOffset(for tool: Tool) -> CGFloat {
    if tool == _model.editedTool {
      return 15
    }
    return tool == _model.selectedTool ? -7 : 4
  }
  
  private func _toggleEdit(of tool: Tool) {
    withAnimation(_editedToolAnimation) {
      _model.editedTool = _model.editedTool == nil ? tool : nil
      onEditTool(_model.editedTool != nil)
    }
  }
  
  private func _changeEditorStyle(_ style: ToolsModel.EditorStyle) {
    withAnimation(.easeOut(duration: 0.25)) {
      _model.editorStyle = style
      onEditorStyleChange(style)
    }
  }
}
