//
//  ContextMenu.swift
//  iPainter
//
//  Created by Artem Shapovalov on 22.10.2022.
//

import SwiftUI

enum MenuItem: Int, CaseIterable, Identifiable {
  var id: Int { rawValue }
  
  case rectangle
  case ellipse
  case bubble
  case star
  case arrow
  case eraser
  case blurEraser
  case objectEraser
  case round
  case arrowTip
  
  var name: String {
    switch self {
    case .rectangle: return "Rectangle"
    case .ellipse:   return "Ellipse"
    case .bubble:    return "Bubble"
    case .star:      return "Star"
    case .arrow:     return "Arrow"
    case .eraser:       return "Eraser"
    case .blurEraser:   return "Blur"
    case .objectEraser: return "Object"
    case .round:        return "Round"
    case .arrowTip:     return "Arrow"
    }
  }
  
  var icon: String {
    switch self {
    case .rectangle: return "shape_rectangle"
    case .ellipse:   return "shape_ellipse"
    case .bubble:    return "shape_bubble"
    case .star:      return "shape_star"
    case .arrow:     return "shape_arrow"
    case .eraser:       return "round"
    case .blurEraser:   return "blur"
    case .objectEraser: return "xmark"
    case .round:        return "round"
    case .arrowTip:     return "arrow"
    }
  }
  
  static var regularShapes: [MenuItem] {
    [.rectangle, .ellipse, .bubble, .star, .arrow]
  }
  
  static var erasers: [MenuItem] {
//    [.eraser, .blurEraser, .objectEraser]
    [.eraser, .objectEraser]
  }
  
  static var penTips: [MenuItem] {
    [.round, .arrowTip]
  }
}

struct ContextMenu: View {
  
  var items: [MenuItem]
  var select: (MenuItem) -> ()
  
  var body: some View {
    ZStack {
      VisualEffectView(effect: UIBlurEffect(style: .dark))
      VStack(spacing: 0) {
        ForEach(items, id: \.id) { i in
          Button {
            select(i)
          } label: {
            VStack(spacing: 0) {
              HStack {
                Text(i.name)
                  .foregroundColor(.white)
                Spacer()
                Image(i.icon)
                  .renderingMode(.original)
                  .resizable()
                  .aspectRatio(contentMode: .fit)
                  .frame(width: 25, height: 25)
              }
              .padding(.horizontal, 11)
              .frame(height: 45)
              if i != .arrow {
                Divider().padding(0)
              }
            }
            .padding(0)
          }
          .padding(0)
        }
      }
    }
  }
}

struct VisualEffectView: UIViewRepresentable {
    var effect: UIVisualEffect?
    func makeUIView(context: UIViewRepresentableContext<Self>) -> UIVisualEffectView {
      UIVisualEffectView()
    }
    func updateUIView(_ uiView: UIVisualEffectView, context: UIViewRepresentableContext<Self>) {
      uiView.effect = effect
    }
}
