//
//  ShapeModel.swift
//  iPainter
//
//  Created by Artem Shapovalov on 27.10.2022.
//

import SwiftUI

enum ShapeType: Int, CaseIterable, Identifiable {
  
  var id: Int { rawValue }
  
  case rectangle
  case elliplse
  case bubble
  case star
  case arrow
}

class ShapeModel: ObservableObject {
  
  @Published var selectedShape: ShapeType? = nil
  @Published var color: UIColor = .white
  @Published var width: CGFloat = 10
  @Published var flipped = false
  @Published var filled  = false
}
