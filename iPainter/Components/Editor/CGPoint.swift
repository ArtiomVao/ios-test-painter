//
//  CGPoint.swift
//  iPainter
//
//  Created by Artem Shapovalov on 27.10.2022.
//

import Foundation

extension CGPoint {
  
  func distance(to: CGPoint) -> CGFloat {
    CGFloat(sqrt((to.x - x) * (to.x - x) + (to.y - y) * (to.y - y)))
  }
}
