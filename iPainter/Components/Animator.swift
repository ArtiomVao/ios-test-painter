//
//  Animator.swift
//  iPainter
//
//  Created by Artem Shapovalov on 16.10.2022.
//

import UIKit

final class Animator: NSObject, UIViewControllerAnimatedTransitioning {
  
  static let duration: TimeInterval = 0.4
  
  private let _type: PresentationType
  private let _gallery: GalleryCollectionViewController
  private let _editor: EditorViewController
  private var _selectedCellImageViewSnapshot: UIView
  private let cellImageViewRect: CGRect
  
  init?(type: PresentationType,
        gallery: GalleryCollectionViewController,
        editor: EditorViewController,
        selectedCellImageViewSnapshot: UIView) {
    self._type    = type
    self._gallery = gallery
    self._editor  = editor
    self._selectedCellImageViewSnapshot = selectedCellImageViewSnapshot
    
    guard let window = _gallery.view.window ?? _editor.view.window,
          let selectedCell = _gallery.selectedCell
    else { return nil }
    
    self.cellImageViewRect = selectedCell.convert(
      selectedCell.bounds,
      to: window
    )
  }
  
  func transitionDuration(
    using transitionContext: UIViewControllerContextTransitioning?
  ) -> TimeInterval {
    return Self.duration
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    let containerView = transitionContext.containerView
    
    guard let toView = _editor.view else {
      transitionContext.completeTransition(false)
      return
    }
    
    containerView.addSubview(toView)
    
    guard
      let window = _gallery.view.window ?? _editor.view.window,
      let editorImage = _editor.imageView.image,
      let _ = _editor.imageView.snapshotView(afterScreenUpdates: true),
      let editorControls = _editor.controlsView.snapshotView(
        afterScreenUpdates: true),
      let editorHeader = _editor.headerView.snapshotView(
        afterScreenUpdates: true)
    else {
      transitionContext.completeTransition(true)
      return
    }
    
    let isPresenting = _type.isPresenting
    let cellImageSnapshot = UIImageView()
    cellImageSnapshot.contentMode = .scaleAspectFill
    cellImageSnapshot.image = editorImage
    
    let backgroundView: UIView
    let fadeView = UIView(frame: containerView.bounds)
    
    fadeView.backgroundColor = _editor.view.backgroundColor
    
    _selectedCellImageViewSnapshot = cellImageSnapshot
    
    if isPresenting {
      backgroundView = UIView(frame: containerView.bounds)
      backgroundView.addSubview(fadeView)
      fadeView.alpha = 0
    } else {
      backgroundView = _gallery.view.snapshotView(afterScreenUpdates: true) ?? fadeView
      backgroundView.addSubview(fadeView)
    }
    
    fadeView.addSubview(editorControls)
    fadeView.addSubview(editorHeader)
    
    toView.alpha = 0
    
    [backgroundView, _selectedCellImageViewSnapshot].forEach {
      containerView.addSubview($0)
    }
    
    let editorImageViewRect = _editor.view.convert(
      _editor.imageView.rectOfImage,
      to: window
    )
    
    _selectedCellImageViewSnapshot
      .frame = isPresenting ? cellImageViewRect : editorImageViewRect
    _selectedCellImageViewSnapshot.layer.masksToBounds = true
    
    editorControls.frame = _editor.view.convert(
      _editor.controlsView.frame,
      to: window
    )
    
    editorHeader.frame = _editor.view.convert(
      _editor.headerView.frame,
      to: window
    )
    
    UIView.animate(withDuration: Self.duration,
                   delay: 0.0,
                   usingSpringWithDamping: isPresenting ? 0.8 : 1.0,
                   initialSpringVelocity: 0.3,
                   options: [.curveEaseInOut]) {
      self._selectedCellImageViewSnapshot
        .frame = isPresenting ? editorImageViewRect : self.cellImageViewRect
    } completion: { _ in
      self._selectedCellImageViewSnapshot.removeFromSuperview()
      transitionContext.completeTransition(true)
    }
    
    UIView.animateKeyframes(withDuration: Self.duration,
                            delay: 0,
                            options: .calculationModeCubic, animations: {
      UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1) {
        fadeView.alpha = isPresenting ? 1 : 0
      }
    }, completion: { _ in
      backgroundView.removeFromSuperview()
      editorControls.removeFromSuperview()
      toView.alpha = 1
    })
  }
}


enum PresentationType {
  
  case present
  case dismiss
  
  var isPresenting: Bool {
    return self == .present
  }
}
