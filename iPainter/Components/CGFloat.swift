//
//  CGFloat.swift
//  iPainter
//
//  Created by Artem Shapovalov on 22.10.2022.
//

import Foundation

extension CGFloat {
  
  static func getYForEllipse(x: CGFloat, a: CGFloat, b: CGFloat) -> CGFloat {
    return sqrt((1 - ((x * x) / (b * b))) * (a * a))
  }
}
