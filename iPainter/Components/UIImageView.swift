//
//  UIImageView.swift
//  iPainter
//
//  Created by Artem Shapovalov on 16.10.2022.
//

import UIKit

extension UIImageView {
  
  var rectOfImage: CGRect {
    let imageViewSize = frame.size
    let imgSize = image?.size
    
    guard let imageSize = imgSize else {
      return CGRect.zero
    }
    
    let scaleWidth  = imageViewSize.width / imageSize.width
    let scaleHeight = imageViewSize.height / imageSize.height
    let aspect = fmin(scaleWidth, scaleHeight)
    
    var imageRect = CGRect(x: 0, y: 0,
                           width: imageSize.width * aspect,
                           height: imageSize.height * aspect)
    // Center image
    imageRect.origin.x = (imageViewSize.width - imageRect.size.width) / 2
    imageRect.origin.y = (imageViewSize.height - imageRect.size.height) / 2
    
    // Add imageView offset
    imageRect.origin.x += frame.origin.x
    imageRect.origin.y += frame.origin.y
    
    return imageRect
  }
}
