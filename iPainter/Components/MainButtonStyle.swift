//
//  MainButtonStyle.swift
//  iPainter
//
//  Created by Artem Shapovalov on 13.10.2022.
//

import SwiftUI

struct MainButtonStyle: ButtonStyle {
  
  func makeBody(configuration: Self.Configuration) -> some View {
    _content(configuration)
      .background(
        RoundedRectangle(cornerRadius: 10, style: .continuous)
          .fill(Color.blue)
      )
      .scaleEffect(configuration.isPressed ? 0.94 : 1)
      .animation(
        configuration.isPressed ? .easeInOut(duration: 0.08) : .none,
        value: configuration.isPressed
      )
  }
  
  @ViewBuilder
  private func _content(_ configuration: Self.Configuration) -> some View {
    HStack {
      Spacer()
      configuration
        .label
        .font(.system(size: 20, weight: .semibold))
        .foregroundColor(.white)
      Spacer()
    }
  }
}

