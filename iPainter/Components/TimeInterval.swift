//
//  TimeInterval.swift
//  iPainter
//
//  Created by Artem Shapovalov on 15.10.2022.
//

import Foundation

extension TimeInterval {
  
  var asDateString: String {
    let formatter = DateComponentsFormatter()
    formatter.allowedUnits = [.minute, .second]
    formatter.unitsStyle = .positional
    formatter.zeroFormattingBehavior = .pad
    return formatter.string(from: self)!
  }
}
