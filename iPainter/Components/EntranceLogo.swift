//
//  SwiftUIView.swift
//  iPainter
//
//  Created by Artem Shapovalov on 29.10.2022.
//

import SwiftUI
import Lottie

struct EntranceLogo: UIViewRepresentable {
  
  func makeUIView(context: Context) -> some UIView {
    
    let contentView = UIView()
    contentView.frame = CGRect(x: 0, y: 0, width: 150, height: 150)
    
    let lottieView = LottieAnimationView(name: "duck")
    
    lottieView.contentMode = .scaleAspectFit
    lottieView.loopMode = .loop
    lottieView.play()
    
    contentView.addSubview(lottieView)
    
    lottieView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
    lottieView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    lottieView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
    lottieView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
    lottieView.translatesAutoresizingMaskIntoConstraints = false
    
    return contentView
  }
  
  func updateUIView(_ uiView: UIViewType, context: Context) {
  }
}
