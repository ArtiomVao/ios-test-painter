//
//  AssetCollectionViewCell.swift
//  iPainter
//
//  Created by Artem Shapovalov on 15.10.2022.
//

import SwiftUI
import Photos

let imageManager = PHCachingImageManager();

class AssetCollectionViewCell: UICollectionViewCell {
  static let idx = "AssetCollectionViewCell"
  var representedAssetIdentifier = ""
  
  let imageView = UIImageView()
  let timeLabel = UILabel()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    imageView.contentMode = .scaleAspectFill
    imageView.clipsToBounds = true
    
    timeLabel.textAlignment = .right
    timeLabel.layer.shadowRadius = 10
    timeLabel.layer.shadowColor = UIColor.black.cgColor
    timeLabel.textColor = .white
    timeLabel.font = .systemFont(ofSize: 14)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func set(asset: PHAsset) {
    representedAssetIdentifier = asset.localIdentifier

    imageManager.requestImage(for: asset,
                              targetSize: CGSize(width: 300, height: 300),
                              contentMode: .default,
                              options: nil) { img, _ in
      if self.representedAssetIdentifier == asset.localIdentifier,
          let img = img {
        self.imageView.image = img
      }
    }

    addSubview(imageView)
    
    imageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
    imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
    imageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    imageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    imageView.translatesAutoresizingMaskIntoConstraints = false
    
    if asset.duration > 0 {
      timeLabel.text = asset.duration.asDateString
      
      addSubview(timeLabel)
      
      timeLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
      timeLabel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
      timeLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -4).isActive = true
      timeLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4).isActive = true
      timeLabel.translatesAutoresizingMaskIntoConstraints = false
    }
  }
}
