//
//  PHAsset.swift
//  iPainter
//
//  Created by Artem Shapovalov on 15.10.2022.
//

import Photos
import UIKit

extension PHAsset {
  
  func getAssetThumbnail(size: CGSize) -> UIImage {
    let manager = PHImageManager.default()
    let option = PHImageRequestOptions()
    var thumbnail = UIImage()
    
    option.isSynchronous = true
    
    manager.requestImage(for: self,
                         targetSize: size,
                         contentMode: .default,
                         options: nil) { result, _ in
      guard let r = result else {
        return
      }
      
      thumbnail = r
    }
    
    return thumbnail
  }
  
  func getOrginalImage(complition: @escaping (UIImage) -> Void) {
    let manager = PHImageManager.default()
    var image = UIImage()
    let options = PHImageRequestOptions()
    options.deliveryMode = .highQualityFormat
    
    manager.requestImage(for: self,
                         targetSize: PHImageManagerMaximumSize,
                         contentMode: .default,
                         options: options) { result, _ in
      guard let r = result else {
        return
      }
      
      image = r
      complition(image)
    }
  }
  
  func getImageFromPHAsset() -> UIImage {
    var image = UIImage()
    let requestOptions = PHImageRequestOptions()
    
    requestOptions.resizeMode    = .exact
    requestOptions.deliveryMode  = .highQualityFormat
    requestOptions.isSynchronous = true
    
    if (mediaType == .image) {
      PHImageManager.default()
        .requestImage(for: self,
                      targetSize: PHImageManagerMaximumSize,
                      contentMode: .default,
                      options: requestOptions) { pickedImage, _ in
          guard let p = pickedImage else {
            return
          }
          image = p
        }
    }
    return image
  }
}
