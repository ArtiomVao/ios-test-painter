//
//  GalleryView.swift
//  iPainter
//
//  Created by Artem Shapovalov on 15.10.2022.
//

import Contacts
import UIKit
import Photos
import SwiftUI

class GalleryCollectionViewController: UIViewController, UINavigationControllerDelegate {
  
  enum AppStage {
    case entrance
    case gallery
    case editor
  }
  
  var rowSize = 3
  var collectionView: UICollectionView = UICollectionView(
    frame: .zero, collectionViewLayout: UICollectionViewFlowLayout()
  )
  var entranceView: UIView?
  var selectedCell: AssetCollectionViewCell?
  var selectedCellImageViewSnapshot: UIView?
  var animator: Animator?
  
  private var _fetchResult: PHFetchResult<PHAsset>? = nil
  
  private var _lastPhotoId: Int? = nil
  private let _fetchAmount = 100
  private var _fetching = false
  private lazy var _contactsButton = {
    UIBarButtonItem(title: "Contacts",
                    style: .plain,
                    target: self,
                    action: #selector(_openContacts))
  }()
  private lazy var _camButton = {
    UIBarButtonItem(image: UIImage(systemName: "camera"),
                    style: .plain,
                    target: self,
                    action: #selector(_openCam))
  }()
  private lazy var _imagePicker: UIImagePickerController = {
      let imagePicker = UIImagePickerController()
      imagePicker.allowsEditing = false
      imagePicker.delegate = self
      return imagePicker
  }()

  init() {
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = .black
    title = "Gallery"
    navigationItem.leftBarButtonItem = _contactsButton
    navigationItem.rightBarButtonItem = _camButton
    navigationController?.navigationBar.prefersLargeTitles = false

    collectionView.backgroundColor = .black
    
    if PHPhotoLibrary.authorizationStatus() != .authorized {
      let entrance = Entrance() {
        DispatchQueue.main.async {
          UIView.animate(withDuration: 0.3) {
            self.entranceView?.alpha = 0
          } completion: { _ in
            self.entranceView?.removeFromSuperview()
            self._addCollectionView()
            self.view.layoutSubviews()
          }
        }
      }
      entranceView = UIHostingController(rootView: entrance).view
      entranceView?.backgroundColor = .black
      if let e = entranceView {
        view.addSubview(e)
        e.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        e.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        e.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        e.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        e.translatesAutoresizingMaskIntoConstraints = false
      }
    } else {
      _addCollectionView()
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setNeedsStatusBarAppearanceUpdate()
  }
  override var preferredStatusBarStyle: UIStatusBarStyle {
    .lightContent
  }
  
  private func _addCollectionView() {
    let l = UICollectionViewFlowLayout()
    let screenWidth = Int(UIScreen.main.bounds.size.width)
    let width = (screenWidth - (rowSize - 1)) / rowSize
    l.itemSize = CGSize(width: width, height: width)
    
    collectionView.setCollectionViewLayout(l, animated: false)
    
    collectionView.delegate   = self
    collectionView.dataSource = self
    collectionView.register(
      AssetCollectionViewCell.self,
      forCellWithReuseIdentifier: AssetCollectionViewCell.idx
    )
    
    view.addSubview(collectionView)
    
    collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    collectionView.translatesAutoresizingMaskIntoConstraints = false
    collectionView.alpha = 0
    
    _fetchPhotos()
  }
  
  private func _fetchPhotos() {
    let allPhotosOptions = PHFetchOptions()
    allPhotosOptions.sortDescriptors = [
      NSSortDescriptor(key: "creationDate", ascending: false)
    ]
    
    _fetchResult = PHAsset.fetchAssets(with: allPhotosOptions)
    UIView.animate(withDuration: 0.3) {
      self.collectionView.alpha = 1
    }
  }
  
  @objc private func _openContacts() {
    let isFirstPermissionRequest = CNContactStore.authorizationStatus(for: .contacts) == .notDetermined
    
    CNContactStore().requestAccess(for: .contacts) { accessGranted, _ in
      DispatchQueue.main.async {
        if accessGranted {
          let contactsVC = ContactsViewController() { [weak self] contact in
            self?.presentEditor(contact: contact)
          }
          self.present(contactsVC, animated: true)
          return
        }
        
        if isFirstPermissionRequest {
          return
        }
        
        self.present(self._getAccessAlert(for: "контактам"), animated: true)
      }
    }
  }
  
  @objc private func _openCam() {
    let isFirstPermissionRequest = AVCaptureDevice.authorizationStatus(for: .video) == .notDetermined
    
    AVCaptureDevice.requestCameraPermission { accessGranted in
      DispatchQueue.main.async {
        if accessGranted {
          self._imagePicker.sourceType = .camera
          self.present(self._imagePicker, animated: true)
          return
        }
        
        if isFirstPermissionRequest {
          return
        }
        
        self.present(self._getAccessAlert(for: "камере"), animated: true)
      } // DispatchQueue.main.async
    } // AVCaptureDevice
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.collectionView.performBatchUpdates({ [weak self] in
      let visibleItems = self?.collectionView.indexPathsForVisibleItems ?? []
      self?.collectionView.reloadItems(at: visibleItems)
    }, completion: { (_) in
    })
  }
  
  func presentEditor(contact: CNContact) {
    guard let data = contact.imageData, let img = UIImage(data: data)
    else { return }
    let editor = EditorViewController()
    editor.contact = contact
    editor.transitioningDelegate = self
    editor.modalPresentationStyle = .fullScreen
    editor.onDownloadComplete = _onDowndloadComplete
    editor.imageView.image = img
    self.present(editor, animated: true)
  }
  
  func presentEditor(with asset: PHAsset) {
    let editor = EditorViewController()
    
    editor.transitioningDelegate = self
    editor.modalPresentationStyle = .fullScreen
    editor.onDownloadComplete = _onDowndloadComplete
    
    if asset.duration > 0 {
      let options: PHVideoRequestOptions = PHVideoRequestOptions()
      options.version = .original
      PHImageManager.default().requestAVAsset(
        forVideo: asset,
        options: options
      ) { (asset, audioMix, info) in
        guard let urlAsset = asset as? AVURLAsset else {
          return
        }
        
        editor.videoURL = urlAsset.url
      }
    }
    
    asset.getOrginalImage { img in
      editor.imageView.image = img
      self.present(editor, animated: true)
    }
  }
  
  func presentEditor(img: UIImage) {
    let editor = EditorViewController()
    editor.transitioningDelegate = self
    editor.modalPresentationStyle = .fullScreen
    editor.onDownloadComplete = _onDowndloadComplete
    editor.imageView.image = img
    editor.collectionNeedsInsert = true
    self.present(editor, animated: true)
  }
  
  private func _onDowndloadComplete(_ needInsert: Bool) {
    let id = IndexPath(row: 0, section: 0)
    _fetchPhotos()
    self.collectionView.reloadData()
    collectionView.scrollToItem(at: id, at: .top, animated: false)
    DispatchQueue.main.async {
      self.selectedCell = self.collectionView.cellForItem(at: id) as? AssetCollectionViewCell
      self.selectedCellImageViewSnapshot = self.selectedCell?.imageView
        .snapshotView(afterScreenUpdates: true)
    }
  }
  
  private func _getAccessAlert(for name: String) -> UIAlertController {
    let title = "Необходим доступ"
    let subtitle = "Откройте доступ к \(name) в найтройках устройства."
    let alert = UIAlertController(
      title: title,
      message: subtitle,
      preferredStyle: .alert
    )
    
    let toSettingsAction = UIAlertAction(
      title: "Открыть настройки",
      style: .default ,
      handler: { _ in
        if let url = URL(string: UIApplication.openSettingsURLString) {
          UIApplication.shared.open(url)
        }
      }
    )
    
    let closeAction = UIAlertAction(
      title: "Закрыть",
      style: .cancel
    )
    
    alert.addAction(toSettingsAction)
    alert.addAction(closeAction)
    alert.preferredAction = toSettingsAction
    
    return alert
  }
}

extension GalleryCollectionViewController: UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView,
                      numberOfItemsInSection section: Int) -> Int {
    if let res = _fetchResult {
      return res.count
    }
    return 0
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(
      withReuseIdentifier: AssetCollectionViewCell.idx, for: indexPath
    ) as? AssetCollectionViewCell
    
    //    cell?.set(asset: assets[indexPath.row])
    if let a = _fetchResult?.object(at: indexPath.item) {
      cell?.set(asset: a)
    }
    
    return cell!
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      didSelectItemAt indexPath: IndexPath) {
    selectedCell = collectionView
      .cellForItem(at: indexPath) as? AssetCollectionViewCell
    selectedCellImageViewSnapshot = selectedCell?.imageView
      .snapshotView(afterScreenUpdates: false)
    
    if let a = _fetchResult?.object(at: indexPath.item) {
      presentEditor(with: a)
    }
  }
}

extension GalleryCollectionViewController: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    let size: CGSize
    
    if let l = collectionViewLayout as? UICollectionViewFlowLayout {
      size = l.itemSize
    } else {
      let screenWidth = Int(UIScreen.main.bounds.size.width)
      let width = (screenWidth - (rowSize - 1)) / rowSize
      size = CGSize(width: width, height: width)
    }
    return size
  }
}

extension GalleryCollectionViewController: UIViewControllerTransitioningDelegate {
  
  func animationController(forPresented presented: UIViewController,
                           presenting: UIViewController,
                           source: UIViewController
  ) -> UIViewControllerAnimatedTransitioning? {
    guard let gallery  = presenting as? GalleryCollectionViewController,
          let editor   = presented as? EditorViewController,
          let snapshot = selectedCellImageViewSnapshot
    else { return nil }
    
    animator = Animator(type: .present,
                        gallery: gallery,
                        editor: editor,
                        selectedCellImageViewSnapshot: snapshot)
    return animator
  }
  
  func animationController(
    forDismissed dismissed: UIViewController
  ) -> UIViewControllerAnimatedTransitioning? {
    guard let editor = dismissed as? EditorViewController,
          let snapshot = selectedCellImageViewSnapshot
    else { return nil }
    
    animator = Animator(type: .dismiss,
                        gallery: self,
                        editor: editor,
                        selectedCellImageViewSnapshot: snapshot)
    return animator
  }
}

extension GalleryCollectionViewController: UIImagePickerControllerDelegate {
  
  func imagePickerController(
      _ picker: UIImagePickerController,
      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]
  ) {
    defer {
      picker.dismiss(animated: true)
    }
    
    guard let img = info[.originalImage] as? UIImage else { return }
    
    DispatchQueue.main.async {
      self.presentEditor(img: img)
    }
  }
}

extension AVCaptureDevice {
  static func requestCameraPermission(completion: @escaping (_ status: Bool) -> Void) {
    AVCaptureDevice.requestAccess(for: .video) { accessGranted in
      guard accessGranted == true
      else {
        completion(false)
        return
      }
      
      DispatchQueue.main.async {
        completion(AVCaptureDevice.authorizationStatus(for: .video) == .authorized)
      }
    }
  }
}

