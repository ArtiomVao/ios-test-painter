//
//  PHPhotoLibrary.swift
//  iPainter
//
//  Created by Artem Shapovalov on 15.10.2022.
//

import Photos

extension PHPhotoLibrary {
  
  static func checkAuthorizationStatus(completion: @escaping (_ status: Bool) -> Void) {
    if PHPhotoLibrary.authorizationStatus() == .authorized {
      completion(true)
    } else {
      PHPhotoLibrary.requestAuthorization { newStatus in
        if newStatus == PHAuthorizationStatus.authorized {
          completion(true)
        } else {
          completion(false)
        }
      }
    }
  }
  
}
