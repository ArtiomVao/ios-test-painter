//
//  AppDelegate.swift
//  iPainter
//
//  Created by Artem Shapovalov on 16.10.2022.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    
    window?.makeKeyAndVisible()
    
    return true
  }
}
